/***********************************************

	Perception for Obstacle Detection from Thanakorn Khamvilai 2019.06.04
        
***********************************************/

// include opencv libraries
#include <opencv2/opencv.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/core/utility.hpp>
#include "opencv2/cudastereo.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>
#include "opencv2/ximgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/stitching.hpp"

// include c++ standard libraries
#include <iostream>
#include <thread>
#include <ctype.h>

#include <darknet.h> // include darknet (yolov3) deep learning framework
#include "detector.hpp" // include my own detector object

#define IS_RECORDING // whether you want to record

#define WIDTH_RESOLUTION 640 // width resolution of the camera
#define HEIGHT_RESOLUTION 480 // height resolution of the camera

// Convert to string
#define SSTR( x ) static_cast< std::ostringstream & >( \
( std::ostringstream() << std::dec << x ) ).str()

bool is_verbose = false; // whether you want to see some texts printing (good for debugging)

int main( int argc, const char** argv ) // main function
{		
	if (is_verbose) std::cout << "detector\n";
	DETECTOR detector = DETECTOR(); // detector object
	cv::namedWindow("Detector", cv::WINDOW_AUTOSIZE); // for visualizing detector result

	int is_detector_init = 0, step_detect = 0; // variables to check if the detector is initialized, and to count to step
	
    cv::Mat Image, frame, frame_original; // image variables
	
	// camera matrix from the camera calibration
	cv::Mat Camera_Matrix = cv::Mat::eye(3, 3, CV_32FC1);
	Camera_Matrix.at<float>(0,0) = 271.86522219;
	Camera_Matrix.at<float>(0,2) = 322.87601999;
	Camera_Matrix.at<float>(1,1) = 271.30300558;
	Camera_Matrix.at<float>(1,2) = 230.66822175;
	
	cv::Mat Distortion_Coefficients = cv::Mat::zeros(1, 5, CV_32FC1);
	Distortion_Coefficients.at<float>(0,0) = -0.31551838;
	Distortion_Coefficients.at<float>(0,1) = 0.10660576;
	Distortion_Coefficients.at<float>(0,2) = -0.000816;
	Distortion_Coefficients.at<float>(0,3) = -0.00073065;
	Distortion_Coefficients.at<float>(0,4) = -0.01633807;
    
	//cv::VideoCapture cap("/media/nvidia/TK_SDCard/TK/videos/left3.avi"); // for testing te detector on the recorded video
	cv::VideoCapture cap("/dev/video1"); // read the camera
	
#ifdef IS_RECORDING
	// For Recording Video //
	cv::VideoWriter video_detector("detector_obs.avi", CV_FOURCC('M','J','P','G'), 10, cv::Size(WIDTH_RESOLUTION, HEIGHT_RESOLUTION)); // recording object for the camera
#endif

    while(1) // main loop
    {
    	if (is_verbose) std::cout << "Start Loop" << std::endl;
    	double timer = (double)cv::getTickCount(); // keeping track to time for one loop
        
        //camera input
        if (is_verbose) std::cout << "Fetching Image" << std::endl;
		if(cap.read(frame_original)) // read the camera	
        {
        	cv::undistort(frame_original, frame, Camera_Matrix, Distortion_Coefficients); // undistort the image
        }
        else
        {
        	std::cout << "Cannot Get Images from Videos" << std::endl;
        	return 1; // terminate if cannot read the camera
        }
		
        // main process
        if (is_verbose) std::cout << "Detection Mode" << std::endl;
        std::cout << "step_detect: " << step_detect << std::endl;
        step_detect++; // counting the detection step

        if (is_detector_init == 0) // if the detector haven't yet initialized
        {
        	detector.Init(frame); // initialize using the first image
        	is_detector_init = 1; // detector is initialized
        }
        detector.Capture(frame); // load image to the detector
        	
        if (is_verbose) std::cout << "Detecting Objects" << std::endl;
        detector.Detect(); // detect the obstacles in the loaded image
        	
        if (is_verbose) std::cout << "Display Detected Objects" << std::endl;
        Image = detector.Get_Image(); // get the detected image (same image with the overlay of detected objects)
        
        float fps = cv::getTickFrequency() / ((double)cv::getTickCount() - timer); // calculate frames per second
        cv::putText(Image, "FPS : " + SSTR(int(fps)), cv::Point(100,50), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,0,255), 2); // overlay text
        
        // obstacle avoidance assistance
        cv::line(Image, cv::Point(WIDTH_RESOLUTION/2,0), cv::Point(WIDTH_RESOLUTION/2,HEIGHT_RESOLUTION), cv::Scalar(0,0,255), 2); // draw a line dividing the image to port and starboard
        int left_objects = 0, right_objects = 0; // variables containing the number of obstacles in each side
        for(int i = 0; i < detector.nboxes_boat; i++) // for each detected obstacle
        {
        	if(detector.bbox_detector[i].x + detector.bbox_detector[i].width/2 > WIDTH_RESOLUTION/2) // if it's in the right hand side
        	{
        		right_objects += 1; // add to the right object
        	}
        	else // if it's in the left hand side
        	{
        		left_objects += 1; // add to the left object
        	}
        }
        char string_buf[256]; // for showing text
        std::sprintf(string_buf, "Port : %d", left_objects);
        cv::putText(Image, string_buf, cv::Point(100,75), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,0,255), 2); // show # of obstacles in the left
        std::sprintf(string_buf, "Starboard  : %d", right_objects);
        cv::putText(Image, string_buf, cv::Point(100,100), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,0,255), 2); // show # of obstacles in the right
        //std::cout << "Objects on the Port: " << left_objects << ", Objects on the Starboard: " << right_objects << std::endl;*/ // for printing the same result to the terminal
        
#ifdef IS_RECORDING
        video_detector.write(Image); // recording the camera
#endif
        cv::imshow("Detector", Image ); // visualizing the camera

        //keyboard command
        if (is_verbose) std::cout << "keyboard command" << std::endl;
        int key = 0;
        key = 0xFF & cv::waitKey(1);
        
        if (is_verbose) std::cout << "keyboard pressed" << std::endl;
        
        // End the program if ESC is used
        if( key == 27 ) 
        {
            break;
        }

        // restart by pressing 'r'
        if( key == 'r' )
        {
            is_detector_init = 0;
            std::cout << "restart" << std::endl;
            continue;
        }
        
        // pause by pressing 'p'
        if( key == 'p')
        {	
        	std::cout << "paused" << std::endl;
    		while(cv::waitKey(1) != 'p')
    		{
    			std::cout << "resume" << std::endl;
    		}
    	}
        
        if (is_verbose) std::cout << "End Loop\n" << std::endl;
    }
    // For safely termination
    cv::destroyWindow("Detector");

    return 0; // terminated
    
    while(1); // Thie loop does nothing, but smiling at you :)
}

/* END */

