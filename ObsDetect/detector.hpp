//
// Created by Thanakorn Khamvilai on 5/30/19.
//

#ifndef SHARK_DETECTOR_HPP
#define SHARK_DETECTOR_HPP

#endif //SHARK_DETECTOR_HPP

// include darknet (yolov3) deep learning framework
#include <darknet.h> 
#include "image.h"

// include opencv libraries
#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>

#include "coco_names.hpp" // list of object names

#define DETECTOR_INTERVAL 1.0 // interval for detection (only for babyducking)

class DETECTOR{ // detector class
public:

    int GPU_index; // the index of GPU
    int classes; // the total number of object classes
    char **names; // names of objects
    image **alphabet; // alphabet
    float thresh; // detection threshold
    float hier_thresh; // another detection threshold
    network *net; // neural network object
    
    image buff[1]; // image buffer
    image buff_letter[1]; // image buffer label
    box *bbox_c; // bounding boxes (x,y) at the center
    int nboxes_boat; // the number detected bounding boxes of boats
    
    cv::Rect *bbox_detector; // bounding boxes (x,y) at the top left corner
    cv::Rect kcf_bbox; // a selected bounding box for the tracker
    
    float task_interval; // second

    DETECTOR(); // constructor
    void Detect(); // a function for detecting
    void Capture(cv::Mat frame); // a function for loading an image
    void UpdateBbox(box bbox); // a function for updating a boundary box for a tracker
    cv::Mat Get_Image(); // a function for getting an overlayed image
    void Init(cv::Mat frame); // a function for an initialization
    void Display(); // a function for displaying an overlayed image
    int Select_Object(cv::Rect bb_ocv); // a function for choosing an object if multiple objects of the same class are detected
};
