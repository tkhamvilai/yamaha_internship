//
// Created by Thanakorn Khamvilai on 5/30/19.
//

#include "detector.hpp"

using namespace cv; // so you don't need to type cv::

extern "C" // since darknet (yolov3) is exclusively written in C
{ 
// All these functions below are for converting an opencv image to a darknet image
// Don't need to worry about the converting details
	IplImage *image_to_ipl(image im) // convert image to IplImage
	{
		int x,y,c;
		IplImage *disp = cvCreateImage(cvSize(im.w,im.h), IPL_DEPTH_8U, im.c);
		int step = disp->widthStep;
		for(y = 0; y < im.h; ++y){
		    for(x = 0; x < im.w; ++x){
		        for(c= 0; c < im.c; ++c){
		            float val = im.data[c*im.h*im.w + y*im.w + x];
		            disp->imageData[y*step + x*im.c + c] = (unsigned char)(val*255);
		        }
		    }
		}
		return disp;
	}

	image ipl_to_image(IplImage* src) // convert IplImage to image
	{
		int h = src->height;
		int w = src->width;
		int c = src->nChannels;
		image im = make_image(w, h, c);
		unsigned char *data = (unsigned char *)src->imageData;
		int step = src->widthStep;
		int i, j, k;

		for(i = 0; i < h; ++i){
		    for(k= 0; k < c; ++k){
		        for(j = 0; j < w; ++j){
		            im.data[k*w*h + i*w + j] = data[i*step + j*c + k]/255.;
		        }
		    }
		}
		return im;
	}

	Mat image_to_mat(image im) // convert image to Mat
	{
		image copy = copy_image(im);
		constrain_image(copy);
		if(im.c == 3) rgbgr_image(copy);

		IplImage *ipl = image_to_ipl(copy);
		Mat m = cvarrToMat(ipl, true);
		cvReleaseImage(&ipl);
		free_image(copy);
		return m;
	}

	image mat_to_image(Mat m) // convert Mat to image
	{
		IplImage ipl = m;
		image im = ipl_to_image(&ipl);
		rgbgr_image(im);
		return im;
	}
}
/* End of converting functions */

DETECTOR::DETECTOR() // detector constructor
{
    char *cfg_file = (char*)"../yolov3/cfg/yolov3.cfg"; // locate the config file
    char *weight_file = (char*)"../yolov3/yolov3.weights"; // locate the weight file
    char *data_file = (char*)"../yolov3/cfg/coco_TK.data"; // locate the data file

    this->GPU_index = 0; // set the GPU to use (0 is the first one)
    cuda_set_device(this->GPU_index); // set GPU device

    this->alphabet = load_alphabet2(); // load alphabet

    list * options = read_data_cfg(data_file); // read the data file
    this->classes = option_find_int(options, (char*)"classes", 20); // get the number of classes from the data file, if not available use the default value (20)
    char *name_list = option_find_str(options, (char*)"names", (char*)"data/names.list"); // get the name of classes from the data file, if not available use the default file (data/names.list)
    this->names = get_labels(name_list); // get the label from the name list

    this->thresh = 0.1; // set detection threshold
    this->hier_thresh = 0.1; // set another detection threshold

    this->net = load_network(cfg_file, weight_file, 0); // load the config and the weight file to the neural network
    set_batch_network(this->net, 1); // set batch size for the neural network, 1 is the default value
    
    this->task_interval = DETECTOR_INTERVAL; //detection interval in second
}

void DETECTOR::Init(cv::Mat frame) // detector initializer
{
	this->buff[0] = mat_to_image(frame); // initialize the image to darknet image
    this->buff_letter[0] = letterbox_image(this->buff[0], this->net->w, this->net->h); // initialize letter box for an image
}

void DETECTOR::Capture(cv::Mat frame)
{
    free_image(this->buff[0]); // clear the darknet image data
    this->buff[0] = mat_to_image(frame); // load the image to darknet image
    letterbox_image_into(this->buff[0], this->net->w, this->net->h, this->buff_letter[0]); // load letter box for an image
}

void DETECTOR::Display()
{    
    show_image(this->buff[0], "Detector", 1); // Display the overlayed image
}

void DETECTOR::Detect()
{
    float nms = .4; // non-maximum suppression threshold. 
    // Info for what it is: https://www.pyimagesearch.com/2014/11/17/non-maximum-suppression-object-detection-python/

    layer l = this->net->layers[this->net->n-1]; // layer of the neural network
    float *X = this->buff_letter[0].data; // letter of the label for the neural network
    network_predict(this->net, X); // prediction step of the neural network

    detection *dets = 0; // detectec objects
    int nboxes = 0; // the number of all detected boxes

    dets = get_network_boxes(this->net, this->buff[0].w, this->buff[0].h, this->thresh, this->hier_thresh, 0, 1, &nboxes); // get detected boxes
    if (nms > 0) do_nms_obj(dets, nboxes, l.classes, nms); // filter the non-maximum suppression

    printf("\nObjects:\n");
    image display = this->buff[0]; // load image to overlay the detected boxes
    draw_detections(display, dets, nboxes, this->thresh, this->names, this->alphabet, this->classes); // overlay the detected boxes

    printf("\nnboxes: %d\n", nboxes);
    
    this->bbox_c = new box[nboxes]; // new array of boxes
    this->nboxes_boat = 0; // the number of boxes that are considered as boats
    
    // this loop filter everything that isn't boat out
    for (int8_t i = 0; i < nboxes; i++) // for each possible detected box
    {
        for (uint8_t j = 0; j < this->classes; j++) // for each possible class
        {
            if (dets[i].prob[j] > this->thresh) // if the prob of box i being the class j is greater than a threshold
            {
            	// check whether the class j is a boat
            	if(this->names[j] == this->names[boat] || this->names[j] == this->names[car] || this->names[j] == this->names[truck] || this->names[j] == this->names[bus] || this->names[j] == this->names[bottle]) // boat, car, truck, bus, and bottle are considered as a boat. (bottle because we can test in the office)
            	{
                	this->bbox_c[nboxes_boat] = dets[i].bbox; // get the box of a boat
                	this->nboxes_boat++; // count the number of boxes that are for a boat
                }
            }
        }
    }
    
    // the loop below shifts the (x,y) from the center to the top left corner
    this->bbox_detector = new cv::Rect[nboxes];
    for (int8_t i = 0; i < this->nboxes_boat; i++)
    {
    	this->bbox_detector[i].x = (int)((this->bbox_c[i].x - this->bbox_c[i].w/2) * this->buff[0].w);
    	this->bbox_detector[i].y = (int)((this->bbox_c[i].y - this->bbox_c[i].h/2) * this->buff[0].h);
    	this->bbox_detector[i].width = (int)(this->bbox_c[i].w * this->buff[0].w);
    	this->bbox_detector[i].height = (int)(this->bbox_c[i].h * this->buff[0].h);
    }
    
    free_detections(dets, nboxes); // clear detections
}

cv::Mat DETECTOR::Get_Image()
{    
    return image_to_mat(this->buff[0]); // output an overlayed image
}

void DETECTOR::UpdateBbox(box bbox) // update the bounding box for a tracker
{
    std::cout << "Update Tracker BBox" << std::endl;
	
	// shifts the (x,y) from the center to the top left corner
    this->kcf_bbox.x = (int)((bbox.x - bbox.w/2) * this->buff[0].w);
    this->kcf_bbox.y = (int)((bbox.y - bbox.h/2) * this->buff[0].h);
    this->kcf_bbox.width = (int)(bbox.w * this->buff[0].w);
    this->kcf_bbox.height = (int)(bbox.h * this->buff[0].h);
	
	// change the size the multiple of 16; otherwise, the tracker fails
	if(kcf_bbox.width<16)
    {
    	kcf_bbox.width=16;
    }
    else
    {
    	if(kcf_bbox.width%16)
        {
        	kcf_bbox.width=(kcf_bbox.width/16)*16;
        }
    }
        
    if(kcf_bbox.height<16)
    {
    	kcf_bbox.height=16;
    }
    else
    {
    	if(kcf_bbox.height%16)
		{
			kcf_bbox.height=(kcf_bbox.height/16)*16;
		}
	}
}

int DETECTOR::Select_Object(cv::Rect bb_ocv) // choosing an object if multiple objects of the same class are detected
{
	int rect_ind = -1, rect_area = 0;
	cv::Rect rect_intersect;
	
	// this loop find the overlapping area between the previous box and all the other boxes
	// then choose the box that has the most overlapping area
	for (int i = 0; i < this->nboxes_boat; i++) // for each boat box
	{
		rect_intersect = this->bbox_detector[i] & bb_ocv; // intersect with the previous box
		if (rect_intersect.width * rect_intersect.height > rect_area) // if the intersection area is greater than the previous index
		{
			rect_area = rect_intersect.width * rect_intersect.height; // take note of the current area
			rect_ind = i; // choose the index of the box containing the greater intersection area
		}
    }
    
    return rect_ind; // output the boat box index
}

