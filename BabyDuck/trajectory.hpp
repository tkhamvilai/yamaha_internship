#include <iostream>
#include <vector>
#include <algorithm>

#define HISTORY_DATA_NUM 10 // the maximum number of points to be stored

class TRAJECTORY{ // trajectory class
public:
	std::vector<float> traj_x; // raw x trajectory
	std::vector<float> traj_y; // raw y trajectory
	std::vector<float> traj_x_process; // processed x trajectory
	std::vector<float> traj_y_process; // processed y trajectory
	
	TRAJECTORY(); // constructor
	void store_point(float pos_x, float pos_y); // storing points
	void print_points(); // print the processed points
	void process_points(int w, int h); // process points
};
