#include <opencv2/opencv.hpp>
#include "../utils/ymc_stereo_camera.h"
#include "../utils/stereo_rectify.h"

void stereoMatching(cv::Mat gray_mat_l, cv::Mat gray_mat_r, cv::Mat *disp); // get depth map
const int getDisparityMaximum(const cv::Mat& disp_map, const cv::Rect& bbox); // get minimum distance. note that max disparity = min distance
const float getDisparityAverage(const cv::Mat& disp_map, const cv::Rect& bbox); // get the average disparity
float getDistanceAverageFromDisparity(const cv::Mat& disp_map, const cv::Rect& bbox, int kDepthMinRangeValue, int kDepthMaxRangeValue); // get the average distance from the disparity
float getDistanceMinimumFromDisparity(const cv::Mat& disp_map, const cv::Rect& bbox, int kDepthMinRangeValue, int kDepthMaxRangeValue); // get the average distance from the disparity
bool getRectifyImage(
        YMCStereoCamera& camera,
        StereoRectify&   stereo_rectify,
        cv::Mat&         left_image,
        cv::Mat&         right_image); // get an image
