#include <opencv2/opencv.hpp>
#include <sl/Camera.hpp>

cv::Mat slMat2cvMat(sl::Mat input);
void fetchImages(cv::Mat *img_mat, cv::Mat *dep_mat, cv::Mat *disp_mat, sl::Camera *zed, sl::ERROR_CODE err, sl::RuntimeParameters runtime_parameters); // get an image
float estimateDistanceAverage(cv::Mat& depth_map, cv::Rect& bbox, int kDepthMinRangeValue, int kDepthMaxRangeValue); // get an average distance
float estimateDistanceMinimum(cv::Mat& depth_map, cv::Rect& bbox, int kDepthMinRangeValue, int kDepthMaxRangeValue, int *x_min, int *y_min); // get a minimum distance
