#include "zed_cam.hpp"

float noise = 0.1;

cv::Mat slMat2cvMat(sl::Mat input) // for converting a ZED image to an opencv image
{
    // Mapping between MAT_TYPE and CV_TYPE
    int cv_type = -1;
    switch (input.getDataType()) 
    {
        case sl::MAT_TYPE_32F_C1: cv_type = CV_32FC1; break;
        case sl::MAT_TYPE_32F_C2: cv_type = CV_32FC2; break;
        case sl::MAT_TYPE_32F_C3: cv_type = CV_32FC3; break;
        case sl::MAT_TYPE_32F_C4: cv_type = CV_32FC4; break;
        case sl::MAT_TYPE_8U_C1: cv_type = CV_8UC1; break;
        case sl::MAT_TYPE_8U_C2: cv_type = CV_8UC2; break;
        case sl::MAT_TYPE_8U_C3: cv_type = CV_8UC3; break;
        case sl::MAT_TYPE_8U_C4: cv_type = CV_8UC4; break;
        default: break;
    }

    // Since cv::Mat data requires a uchar* pointer, we get the uchar1 pointer from sl::Mat (getPtr<T>())
    // cv::Mat and sl::Mat will share a single memory structure
    return cv::Mat(input.getHeight(), input.getWidth(), cv_type, input.getPtr<sl::uchar1>(sl::MEM_CPU)); // output opencv image
}

void fetchImages(cv::Mat *img_mat, cv::Mat *dep_mat, cv::Mat *disp_mat, sl::Camera *zed, sl::ERROR_CODE err, sl::RuntimeParameters runtime_parameters) // get an image
{
    sl::Resolution image_size = zed->getResolution(); // get resolution
    
    sl::Mat image_for_display(image_size, sl::MAT_TYPE_8U_C4); // an image for object detection and tracking
    sl::Mat depth_for_display(image_size, sl::MAT_TYPE_32F_C4); // depth map for display (pixel data)
    sl::Mat depth_for_measure(image_size, sl::MAT_TYPE_32F_C1); // depth map for controller (distance data)

    err = zed->grab(runtime_parameters); // evaluate camera parameters

    err = zed->retrieveImage(image_for_display, sl::VIEW_LEFT); // use the left camera for an image
    *img_mat = slMat2cvMat(image_for_display); // convert to an opencv image

	err = zed->retrieveImage(depth_for_display, sl::VIEW_DEPTH); // get the depth map for display (pixel data)
    *dep_mat = slMat2cvMat(depth_for_display); // convert to an opencv image

    err = zed->retrieveMeasure(depth_for_measure, sl::MEASURE_DEPTH); // get the depth map for controller (distance data)
    *disp_mat = slMat2cvMat(depth_for_measure); // convert to an opencv image
}

// these two functions below have the same idea of getting a distance which is dividing the bounding box into 3x3 panels, then consider only the middle. 
// The exception between the two is one uses the average of all points in that panel, while the other one uses only a single point that gives the minimum distance.
// Both of them also add a little noise to the result, so the controller doesn't think it's receiving the same data.

float estimateDistanceAverage(cv::Mat& depth_map, cv::Rect& bbox, int kDepthMinRangeValue, int kDepthMaxRangeValue)
{
	if(depth_map.empty())
	{
		std::cout << "Empty Depth" << std::endl;
		return 0.f;
    }
            
    float distance = 0;
    float tmp;
    int pix_count = 0;

    // set Calculate Range (divide a boundary box)
    const int y0 = bbox.y + static_cast<int>(bbox.height / 3);
    const int y1 = bbox.y + static_cast<int>(bbox.height * 2 / 3);
    const int x0 = bbox.x + static_cast<int>(bbox.width / 3);
    const int x1 = bbox.x + static_cast<int>(bbox.width * 2 / 3);
    
    /*std::cout << y0 << std::endl;
    std::cout << y1 << std::endl;
    std::cout << x0 << std::endl;
    std::cout << x1 << std::endl;
    std::cout << depth_map.rows << std::endl;
    std::cout << depth_map.cols << std::endl;*/
	
    // calculate Average
    for (int y = y0; y < y1; y++)
    {
        if (y < 0)continue;
        if (y > depth_map.rows) break;

        for (int x = x0; x < x1; x++)
        {
            if (x < 0) continue;
            if (x > depth_map.cols) break;
            
            /*std::cout << "x: " << x << " of " << x1 << std::endl;
   			std::cout << "y: " << y << " of " << y1 << std::endl;
    		std::cout << "depth: " << depth_map.at<float>(y,x) << std::endl;*/
			
            tmp = depth_map.at<float>(y,x);

            if (0 < tmp && tmp < kDepthMaxRangeValue)
            {
                distance += tmp;
                pix_count++;
            }
        }
    }

    if (pix_count == 0) return 0.f;
    
    if (distance/pix_count > kDepthMaxRangeValue) distance = 0;
    
    //std::cout << "dist: " << distance << ", pix: " << pix_count << std::endl;

    return static_cast<float>(distance/pix_count);
}

float estimateDistanceMinimum(cv::Mat& depth_map, cv::Rect& bbox, int kDepthMinRangeValue, int kDepthMaxRangeValue, int *x_min, int *y_min)
{
	if(depth_map.empty())
	{
		std::cout << "Empty Depth" << std::endl;
		return 0.f;
    }

    float distance = kDepthMaxRangeValue;
    float tmp;

    // set Calculate Range (divide a boundary box)
    const int y0 = bbox.y + static_cast<int>(bbox.height / 3);
    const int y1 = bbox.y + static_cast<int>(bbox.height * 2 / 3);
    const int x0 = bbox.x + static_cast<int>(bbox.width / 3);
    const int x1 = bbox.x + static_cast<int>(bbox.width * 2 / 3);

    // calculate Minimum
    for (int y = y0; y < y1; y++)
    {
        if (y < 0)continue;
        if (y > depth_map.rows) break;

        for (int x = x0; x < x1; x++)
        {
            if (x < 0) continue;
            if (x > depth_map.cols) break;

            tmp = depth_map.at<float>(y,x);

            if (0 < tmp && tmp < kDepthMaxRangeValue)
            {
                if (distance > tmp)
                {
                    distance = tmp;
                    *x_min = x;
                    *y_min = y;
                }
            }
        }
    }
        
    if (distance == kDepthMaxRangeValue)
        distance = 0.0f;
        
    if (distance > kDepthMaxRangeValue) distance = 0.0;

	noise += 0.1;
	if(noise == 1.0) noise = 0.1;
    return distance + noise;
}
