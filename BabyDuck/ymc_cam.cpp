#include "ymc_cam.hpp"

// Stereo Matching Parameter
constexpr int kStereoWinSize = 21;
constexpr int kStereoDisparityNum = 128;

// YMC Camera Parameters //
const float baseLine = 300.0;
const float focalLength = 5.6;
const float pixSize = 0.006081;
const float distance_coeff = baseLine*focalLength/pixSize; //276270.350271
float dist_noise = 0.1;

void stereoMatching(cv::Mat gray_mat_l, cv::Mat gray_mat_r, cv::Mat *disp) // use opencv3, not compatible with opencv2
{
    cv::cuda::GpuMat left_gray_gpu(gray_mat_l.rows, gray_mat_l.cols, CV_8UC1); // gpu left image
    cv::cuda::GpuMat right_gray_gpu(gray_mat_r.rows, gray_mat_r.cols, CV_8UC1); // gpu right image
    cv::cuda::GpuMat disparity_gpu(gray_mat_l.rows, gray_mat_l.cols, CV_8UC1); // gpu disparity image
    cv::cuda::GpuMat disparity_gpu_filtered(gray_mat_l.rows, gray_mat_l.cols, CV_8UC1); // gpu filtered disparity image

    left_gray_gpu.upload( gray_mat_l ); // load cpu to gpu for left image
    right_gray_gpu.upload( gray_mat_r ); // load cpu to gpu for right image

    // stereo matching
    cv::Ptr<cv::StereoBM> bm = cv::cuda::createStereoBM(kStereoDisparityNum, kStereoWinSize); // create stereo matching object
    bm->compute(left_gray_gpu, right_gray_gpu, disparity_gpu); // compute disparity map
    
    // filtering
    //cv::Ptr<cv::cuda::DisparityBilateralFilter> d_filter = cv::cuda::createDisparityBilateralFilter(kStereoDisparityNum, kStereoWinSize);
    //d_filter->apply(disparity_gpu, left_gray_gpu, disparity_gpu_filtered);

    // GpuMat --> cv::Mat
    disparity_gpu.download(*disp); // load gpu disparity map to cpu
    //disparity_gpu_filtered.download(*disp);   
}

const int getDisparityMaximum(const cv::Mat& disp_map, const cv::Rect& bbox) // max disparity = min distance
{
    //float disparity = kStereoDisparityNum;
    int disparity = 0;
	int tmp;
	
	// set Calculate Range (divide into 6x3 panels)
	const int y0 = bbox.y + static_cast<int>(bbox.height / 2);
	const int y1 = bbox.y + static_cast<int>(bbox.height * 5 / 6); // use vertical panels of 4 and 5
	const int x0 = bbox.x + static_cast<int>(bbox.width / 3);
	const int x1 = bbox.x + static_cast<int>(bbox.width * 2 / 3); // use the middle horizontal panel
	
	// calculate Minimum
	for (int y = y0; y < y1; y++)
	{
		if (y < 0)continue;
		if (y > disp_map.rows) break;
		
		for (int x = x0; x < x1; x++)
		{
			if (x < 0) continue;
			if (x > disp_map.cols) break;
			
			tmp = static_cast<int>( disp_map.at<uchar>(y,x) );
			
			if (0 < tmp && tmp < kStereoDisparityNum)
			{
				if (disparity < tmp)
					disparity = tmp;
			}
		}
	}
	
	return disparity;
}

const float getDisparityAverage(const cv::Mat& disp_map, const cv::Rect& bbox)
{
    //float disparity = kStereoDisparityNum;
    uintmax_t sum = 0;
    uintmax_t cnt = 0;
    int       tmp;

    // set Calculate Range (divide into 6x3 panels)
    const int y0 = bbox.y + static_cast<int>(bbox.height / 2);
    const int y1 = bbox.y + static_cast<int>(bbox.height * 5 / 6); // use vertical panels of 4 and 5
    const int x0 = bbox.x + static_cast<int>(bbox.width / 3);
    const int x1 = bbox.x + static_cast<int>(bbox.width * 2 / 3); // use the middle horizontal panel

    // calculate Minimum
    for (int y = y0; y < y1; y++)
    {
        if (y < 0)continue;
        if (y > disp_map.rows) break;

        for (int x = x0; x < x1; x++)
        {
            if (x < 0) continue;
            if (x > disp_map.cols) break;

            tmp = static_cast<int>(disp_map.at<uchar>(y, x));

            if (0 < tmp && tmp < kStereoDisparityNum)
            {
                sum += tmp;
                cnt++;
            }
        }
    }

    if (cnt == 0) return 0; // if not enough pixels, output zero

    return static_cast<float>(sum) / cnt; // output average
}

// get the average distance from the disparity
float getDistanceAverageFromDisparity(const cv::Mat& disp_map, const cv::Rect& bbox, int kDepthMinRangeValue, int kDepthMaxRangeValue)
{
	dist_noise += 0.1;
    if(dist_noise == 1.0) dist_noise = 0.1;
    
	float dist = 0.0;
	dist = getDisparityAverage(disp_map, bbox);
	
	if (dist <= 0) dist = 0.1;
	else dist = distance_coeff/dist; // inverse the disparity
	
	if (dist > kDepthMaxRangeValue) dist = 0.0; // if too far, send zero
	return dist + dist_noise; // output distance, add a little noise, so the controller doesn't think it keeps getting the same value
}

// get the average distance from the disparity
float getDistanceMinimumFromDisparity(const cv::Mat& disp_map, const cv::Rect& bbox, int kDepthMinRangeValue, int kDepthMaxRangeValue)
{
	dist_noise += 0.1;
    if(dist_noise == 1.0) dist_noise = 0.1;
    
	float dist = 0.0;
	dist = getDisparityMaximum(disp_map, bbox);
	
	if (dist <= 0) dist = 0.1;
	else dist = distance_coeff/dist; // inverse the disparity
	
	if (dist > kDepthMaxRangeValue) dist = 0.0; // if too far, send zero
	return dist + dist_noise; // output distance, add a little noise, so the controller doesn't think it keeps getting the same value
}

bool getRectifyImage( // this function pass an image by reference
        YMCStereoCamera& camera,
        StereoRectify&   stereo_rectify,
        cv::Mat&         left_image,
        cv::Mat&         right_image)
{
	cv::Mat left_tmp(camera.height(), camera.width(), CV_8U);
	cv::Mat right_tmp(camera.height(), camera.width(), CV_8U);
	
	left_image.create(left_tmp.size(), CV_8U);
	right_image.create(left_tmp.size(), CV_8U);
	
	bool ret;
	
	ret = camera.getImage( left_tmp.ptr<uchar>(0),
	                       right_tmp.ptr<uchar>(0),
	                       left_tmp.cols,
	                       left_tmp.rows);
	if (ret == false) return false;
	
	ret = stereo_rectify.process( left_tmp.ptr<uchar>(0),
	                              right_tmp.ptr<uchar>(0),
	                              left_image.ptr<uchar>(0),
	                              right_image.ptr<uchar>(0),
	                              left_image.cols,
	                              right_image.rows);
    if (ret == false) return false;
    
    return true;
}
