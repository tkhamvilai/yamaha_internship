/***********************************************

	Perception for Baby Duck from Thanakorn Khamvilai 2019.06.04
	
	Main Loop

	  start
	    |
	    |                                   
	    v                                  
	  detect <--------------<---------------
	    |				   |               |
	    |                  ^ no            |
	    v                  |               |
    has target?  ------>---                |
        |                       every pre-specify seconds
        |yes                               |
        v                                  |
     tracking                              |   
        |                                  |
        --------->------------------->------
        
***********************************************/

// include opencv libraries
#include <opencv2/opencv.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/core/utility.hpp>
#include "opencv2/cudastereo.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>
#include "opencv2/ximgproc.hpp"

// include c++ standard libraries
#include <iostream>
#include <thread>
#include <ctype.h>

#include <darknet.h> // include darknet (yolov3) deep learning framework

// include other files
#include "../ObsDetect/detector.hpp" // detector
#include "ymc_cam.hpp" // api for reading a GC camera
#include "zed_cam.hpp" // api for reading a ZED camera
#include "trajectory.hpp" // trajectory generation
#include "../kcf_src/kcf.h" // tracker
#include "../utils/udp_driver.h" // udp
#include "../utils/ymc_stereo_camera.h" // GC camera driver
#include "../utils/stereo_rectify.h" // GC camera rectifier
#include <sl/Camera.hpp> // ZED camera driver

#define IS_RECORDING // whether you want to record
//#define SHOW_DEPTH // whether the depth map is displayed
#define UDP   // UDP or not

////////////////////////////////////////
// !!!! Choose Only One !!!!

#define YMC_CAM // use GC camera

//#define ZED_CAM_HD2K // use ZED with HD2K resolution
//#define ZED_HD_1080 // use ZED with HD1080 resolution
//#define ZED_HD_720 // use ZED with HD720 resolution
//#define ZED_HD_VGA // use ZED with VGA resolution

//#define USE_VIDEO // use the recorded video
////////////////////////////////////////

#if defined( YMC_CAM )
	#define WIDTH_RESOLUTION 640 // GC camera width resolution
	#define HEIGHT_RESOLUTION 480 // GC camera height resolution
	// Rectify Data
	const std::string kStereoRectifyFilePath = "../src/map_pg11_190717.dat"; // GC camera calibration data
#endif

// Define ZED camera width and height resolution
#if defined( ZED_CAM_HD2K ) || defined( ZED_HD_1080 ) || defined( ZED_HD_720 ) || defined( ZED_HD_VGA )
	#define ZED_CAM
	#if defined ( ZED_CAM_HD2K )
		#define WIDTH_RESOLUTION 2048
		#define HEIGHT_RESOLUTION 1080	
	#elif defined ( ZED_HD_1080 )
		#define WIDTH_RESOLUTION 1920
		#define HEIGHT_RESOLUTION 1080	
	#elif defined ( ZED_HD_720 )
		#define WIDTH_RESOLUTION 1280
		#define HEIGHT_RESOLUTION 720	
	#elif defined ( ZED_HD_VGA )
		#define WIDTH_RESOLUTION 640
		#define HEIGHT_RESOLUTION 480
	#endif
#endif

// Define recorded video width and height resolution
#if defined( USE_VIDEO )
	#define WIDTH_RESOLUTION 640
	#define HEIGHT_RESOLUTION 480
#endif

//tracking mode
#define MODE_DETECTING  1
#define MODE_TRACKING   2

//Selection mode
#define SELECT_NONE 0
#define SELECT_DETECTED_MODE 1
#define SELECT_TARGET_MODE 2

#define NON_SELECT  3
#define END_SELECT  4

// Convert to string
#define SSTR( x ) static_cast< std::ostringstream & >( \
( std::ostringstream() << std::dec << x ) ).str()

bool is_verbose = true; // whether you want to see some texts printing (good for debugging)

// show result mode
enum
{
    SHOW_NON = 0,
    SHOW_DETECT_ON = 1,
    SHOW_DETECT_OFF = 2,
};

// Resize parameter
constexpr float kResizeImageParameter = 1.0/5;	// range value (0.0, 1.0]

//drawing param
const int kLineThickness = 2;
cv::Scalar detect_color(255,0,0);
cv::Scalar select_color(0,255,0);
cv::Scalar tracker_color(0,0,255);

// Depth Range
constexpr int kDepthMaxRangeValue = 30000;	// [mm]
constexpr int kDepthMinRangeValue = 300;	// [mm]

int select_flag = NON_SELECT;
cv::Point select_point;

float dist, p_dist = 0.0f, pos_x, pos_y; // coordinate data to send to UDP

UDPSocket udp; // udp object

// Mouse Callback Code for detection, mouse click event
static void onMouseDetect( int event, int x, int y, int, void* )
{
    switch( event )
    {
        case CV_EVENT_LBUTTONDOWN:
            select_point = cv::Point(x,y);
            std::cout << "Clicked" << std::endl;
            break;

        case CV_EVENT_LBUTTONUP:
        	select_flag = END_SELECT;
        	std::cout << "Released" << std::endl;
            break;
    }
}

int main( int argc, const char** argv ) // main function
{		
	if (is_verbose) std::cout << "detector\n";
	KCF_Tracker tracker; //tracking
	DETECTOR detector = DETECTOR(); // detector
	TRAJECTORY trajectory1 = TRAJECTORY(); // left trajectory
	TRAJECTORY trajectory2 = TRAJECTORY(); // right trajectory
	int is_detector_init = 0, is_tracker_init = 0; // variables to check whether the detector, tracker are initialized
	int target_ind = 0; // index of the target mama duck
	int step_detect = 0; // step of detection
	
    cv::Mat Image, left_image, right_image, depth, frame, frameA, resize_img, disp; // image variables
    cv::Rect selection, target, bb_ocv; // bounding box variables
    int mode = MODE_DETECTING; // operating mode (this is gonna switch between detecting and tracking)
    
    cv::namedWindow("Detector", cv::WINDOW_AUTOSIZE); // visualizing the detector
    cv::setMouseCallback("Detector", onMouseDetect, 0); // set interrupt event
    
#if defined ( YMC_CAM )
    // GC Stereo Camera   //
	YMCStereoCamera camera; // GC camera object
	if (camera.open() == false) // read the GC camera
	{
		std::cout << "could not open camera\n";
		return 1;
	}
	
	// GC Stereo Rectify  //
	StereoRectify stereo_rectify(camera.width(), camera.height()); // GC camera rectifier object
	if (stereo_rectify.readMap(kStereoRectifyFilePath) == false) // rectify GC camera
	{
		std::cout << "could not read rectify data: " << kStereoRectifyFilePath << "\n";
		return 1;
	}
#elif defined ( ZED_CAM )
	sl::ERROR_CODE err; // err status variable
	sl::RuntimeParameters runtime_parameters; // runtime parameter variable
	
    // Create a ZED camera object
    sl::Camera zed;

    //ZED initialize and Params
    sl::InitParameters init_params;

    //image size
    #if defined ( ZED_CAM_HD2K )
		init_params.camera_resolution = sl::RESOLUTION_HD2K;
	#elif defined ( ZED_HD_1080 )
		init_params.camera_resolution = sl::RESOLUTION_HD1080;
	#elif defined ( ZED_HD_720 )
		init_params.camera_resolution = sl::RESOLUTION_HD720;
	#elif defined ( ZED_HD_VGA )
		init_params.camera_resolution = sl::RESOLUTION_VGA;
	#endif

    //If set to 0, the highest FPS of the specified camera_resolution will be used.
    init_params.camera_fps = 0;

    //init_params.depth_mode = sl::DEPTH_MODE_NONE;
    //init_params.depth_mode = sl::DEPTH_MODE_ULTRA;
    init_params.depth_mode = sl::DEPTH_MODE_PERFORMANCE;
    //init_params.depth_mode = sl::DEPTH_MODE_MEDIUM;
    //init_params.depth_mode = sl::DEPTH_MODE_QUALITY;
    //init_params.depth_mode = sl::DEPSocket udp1; //PTH_MODE_ULTRA;

    //Regions of the generated depth map can oscillate from one frame to another.
    //These oscillations result from a lack of texture (too homogeneous) on an object and by image noise.
    //This parameter enables a stabilization filter that reduces these oscillations.
    init_params.depth_stabilization = true;

    //distance unit
    init_params.coordinate_units = sl::UNIT_MILLIMETER;
    //init_params.coordinate_units = UNIT_METER;

    //min range
    init_params.depth_minimum_distance = kDepthMinRangeValue;

    //runtime_parameters.sensing_mode = sl::SENSING_MODE_FILL;
    runtime_parameters.sensing_mode = sl::SENSING_MODE_STANDARD;
    //runtime_parameters.sensing_mode = sl::SENSING_MODE_RAW;

    err = zed.open(init_params); // read ZED camera
    std::cout <<"zed opened\n";

    if (err != sl::SUCCESS)
    {
        std::cout << sl::errorCode2str(err) << std::endl;
        zed.close();
        return EXIT_FAILURE; // quit if an error occurred
    }

    //max range in mm
    zed.setDepthMaxRangeValue( 30000 ); //used to be 10000 	 
    
#elif defined ( USE_VIDEO )
	cv::VideoCapture cap_L("/media/nvidia/D0AE-3A88/videos/left3.avi"); // path to the exisitng left video
	cv::VideoCapture cap_R("/media/nvidia/D0AE-3A88/videos/right3.avi"); // path to the exisitng right video
#endif
	
#ifdef IS_RECORDING
	// For Recording Video //
	cv::VideoWriter video_detector("detector.avi", CV_FOURCC('M','J','P','G'), 30, cv::Size(WIDTH_RESOLUTION, HEIGHT_RESOLUTION)); // recording object for the detector
	cv::VideoWriter video_tracker("tracker.avi", CV_FOURCC('M','J','P','G'), 30, cv::Size(WIDTH_RESOLUTION, HEIGHT_RESOLUTION)); // recording object for the tracker
	cv::VideoWriter video_depth("depth.avi", CV_FOURCC('M','J','P','G'), 30, cv::Size(WIDTH_RESOLUTION, HEIGHT_RESOLUTION)); // recording object for the depth map
#endif

    while(1) // main loop
    {
    	if (is_verbose) std::cout << "Start Loop" << std::endl;
    	double timer = (double)cv::getTickCount(); // keeping track to time for one loop
        
        //camera input
        if (is_verbose) std::cout << "Fetching Image" << std::endl;
        
#if defined ( YMC_CAM )
        if(getRectifyImage(camera, stereo_rectify, left_image, right_image) == true) // get GC camera image
        {	        
        	if (is_verbose) std::cout << "Converting Image" << std::endl; 
        	cv::cvtColor(left_image, frame, cv::COLOR_GRAY2BGR); // convert from grayscale to color (change the image type, not actually put color on it)
        	
        	if (is_verbose) std::cout << "Getting Disp" << std::endl; 
	    	stereoMatching(left_image, right_image, &disp); // get depth map (store in disp variable)
	    	
	    	if (is_verbose) std::cout << "Cloning Image" << std::endl;
        	Image = frame.clone(); // clone from frame to Image
        }
        else
        {
        	//continue;
        	std::cout << "Cannot Get Images from Camera" << std::endl;
        	return 1; // terminate if cannot read camera
        }      
#elif defined ( ZED_CAM )
		fetchImages(&frameA, &depth, &disp, &zed, err, runtime_parameters); // get ZED camera image and depth map
		//disp stores distance data for sending to UDP, while depth stores pixel data to visualize
        
        if(frameA.empty() || disp.empty()) // warning if the picture or depth map is empty
        {
        	if(frameA.empty())
        	{
        		std::cout << "Empty Image" << std::endl;
        	}
        	else if(disp.empty())
        	{
        		std::cout << "Empty distance" << std::endl;
        	}
        	continue;
        }
        
        if (is_verbose) std::cout << "Converting Image" << std::endl;        
        cv::cvtColor(frameA, frame, cv::COLOR_BGRA2BGR); // remove alpha channel  
        
        if (is_verbose) std::cout << "Cloning Image" << std::endl;
        Image = frame.clone(); // clone from frame to Image	
#elif defined ( USE_VIDEO )
		if(cap_L.read(left_image) == true && cap_R.read(right_image)) // read videos
        {	        
        	frame = left_image.clone(); // clone from frame to Image
        	
        	if (is_verbose) std::cout << "Converting Image" << std::endl; 
        	cv::cvtColor(left_image, left_image, cv::COLOR_BGR2GRAY); // convert color to gray for left image
        	cv::cvtColor(right_image, right_image, cv::COLOR_BGR2GRAY); // convert color to gray for right image
        	
        	if (is_verbose) std::cout << "Getting Disp" << std::endl; 
	    	stereoMatching(left_image, right_image, &disp); // get depth map (store in disp variable)
	    	// stereoMatching accepts only grayscale image
	    	
	    	if (is_verbose) std::cout << "Cloning Image" << std::endl;
        	Image = frame.clone(); // clone from frame to Image	        	
        }
        else
        {
        	//continue;
        	std::cout << "Cannot Get Images from Videos" << std::endl;
        	return 1; // terminate if cannot read camera
        }   
#endif  
		
        //main process
        if (is_verbose) std::cout << "Mode Processing" << std::endl;
        if (mode == MODE_DETECTING) // detection mode
        {	
        	std::cout << "step_detect: " << step_detect << std::endl;
        	step_detect++; // count the detection step
        	
        	if (is_verbose) std::cout << "Detection Mode" << std::endl;
        	target_ind = 0; // reset the target index
        	if (is_detector_init == 0) // if the detector haven't yet initialized
        	{
        		detector.Init(frame); // initialize it
        	}
        	detector.Capture(frame); // load image to the detector
        	
        	if (is_verbose) std::cout << "Detecting Objects" << std::endl;
        	detector.Detect(); // detect the objects
        	
        	if (is_verbose) std::cout << "Display Detected Objects" << std::endl;
        	detector.Display(); // display the detection result
        	
        	// check if objects detected
        	std::cout << "Number of Detected Objects: " << detector.nboxes_boat << std::endl;
        	if (is_verbose) std::cout << "is_detector_init: " << is_detector_init << std::endl;
        	if (is_detector_init == 0) // if the detector is haven't yet initialized
        	{
        		if (detector.nboxes_boat <= 0) // if nothing is detected
        		{
        			continue; // go back to get a new image from a camera and detect again
        		}
        		else if (detector.nboxes_boat > 1) // if there are more than one boats detected, let the user choose which one to track 
            	{	
            		if (is_verbose) std::cout << "Choose Target" << std::endl;
					
					if (select_flag != END_SELECT) // if the user haven't chosen yet
            		{
						continue; // go back to get a new image from a camera and detect again
					}									
            	
                	//select target (clicked)                	
                	if (select_point.x > 0 || select_point.y > 0) // check if the user click on the valid target
                	{
                    	int x = select_point.x; // x-coordinate
                    	int y = select_point.y; // y-coordiante
                    	for (int i = 0; i < detector.nboxes_boat; i++) // for each detected boat box
                    	{
                        	if (x > detector.bbox_detector[i].x && x < detector.bbox_detector[i].x + detector.bbox_detector[i].width) // if the clicked x coordinate in inside the box
                        	{
                            	if (y > detector.bbox_detector[i].y && y < detector.bbox_detector[i].y + detector.bbox_detector[i].height) // if the clicked y coordinate in inside the box
                            	{
                            		if (is_verbose) std::cout << "Start Initializing Target: " << i+1 << std::endl;
                                    target_ind = i; // choose that target               
                                	break;
                            	}
                        	}
                        	else if(i == detector.nboxes_boat - 1) // none of detected objects contain the clicked point
                        	{
								if (is_verbose) std::cout << "Invalid Target, Choose Again" << std::endl;
								select_flag = NON_SELECT;
								break;
                        	}
                    	}
                	}
                	
                	if(select_flag != END_SELECT) // if the user haven't chosen yet
            		{
						continue; // go back to get a new image from a camera and detect again
					}	            						
            	}
            	
            	if (is_verbose) std::cout << "Target " << target_ind + 1 << " Chosen" << std::endl;
            	is_detector_init = 1; // the user selected the target or only on target detected, choose that one to be tracked      			
				detector.UpdateBbox(detector.bbox_c[target_ind]); // update the bounding box for tracker
            }
            else if (is_detector_init == 1) // if the detector is already initialized
            {
            	if (detector.nboxes_boat > 1) // more than one boats
            	{	
            		target_ind = detector.Select_Object(bb_ocv); // choose the one that has the most intersection area with the previous bb_ocv
            		if (is_verbose) std::cout << "Target: " << target_ind + 1  << " still be chosen" << std::endl;            		
            	}
            	
            	// once the target is chosen or there is only one target detected, update the bounding box for tracker
            	if (target_ind != -1) detector.UpdateBbox(detector.bbox_c[target_ind]);
            }
			
			// Only one object detected or Target chosen, track it !!!
            if (detector.nboxes_boat > 0) // if no objects detected, keep tracking...
            {	            	
                selection = detector.kcf_bbox; // bounging box for fck tracker
                target = cv::Rect(selection.x*kResizeImageParameter,
                                  selection.y*kResizeImageParameter,
                                  selection.width*kResizeImageParameter,
                                  selection.height*kResizeImageParameter); // make a bounding for target, the size is adjusted by kResizeImageParameter
                
                cv::resize(frame, resize_img, cv::Size(), kResizeImageParameter, kResizeImageParameter); // also resize the image
                        
                if (is_verbose) std::cout << "Initializing Target"<< std::endl;  
                tracker = KCF_Tracker(); // reconstruct the tracker object
                tracker.init(resize_img, target); // reinitialized the kfc tracker
            }
            
            // trajectory generation
            trajectory1.store_point(detector.bbox_detector[target_ind].x, 
        						   detector.bbox_detector[target_ind].y + detector.bbox_detector[target_ind].height); // store lower left corner
        	trajectory1.process_points(WIDTH_RESOLUTION, HEIGHT_RESOLUTION); // process stored points
        	trajectory1.print_points(); // print the processed points
        	
        	trajectory2.store_point(detector.bbox_detector[target_ind].x + detector.bbox_detector[target_ind].width, 
        						   detector.bbox_detector[target_ind].y + detector.bbox_detector[target_ind].height); // store lower right corner        	
        	trajectory2.process_points(WIDTH_RESOLUTION, HEIGHT_RESOLUTION); // process stored points
        	trajectory2.get_points(); // get the processed points
                
            mode = MODE_TRACKING; // finish detecting, start tracking
        }
        else if(mode == MODE_TRACKING)
        {			
			float time = timer/cv::getTickFrequency(); // keep track of a time
			if (is_verbose) std::cout << "time: " << time << std::endl;
			
			if (time > detector.task_interval) // if the time is greater than the detector interval
			{
				if (is_verbose) std::cout << "Change to Detection Mode" << std::endl;
				detector.task_interval = time + DETECTOR_INTERVAL; // shift the interval by DETECTOR_INTERVAL (currently 1 sec)
        		mode = MODE_DETECTING; // mode is changed to detection
        	}

            //tracking
            cv::resize(frame, resize_img, cv::Size(), kResizeImageParameter, kResizeImageParameter); // resize the tracking image
            if(frame.empty() || resize_img.empty()) // is there a image to be tracked?
            {
            	std::cout << "Empty Image to Track" << std::endl;
            }
            if (is_verbose) std::cout << "Tracking Mode" << std::endl;
            tracker.track(resize_img); // actually do tracking!
            if (is_verbose) std::cout << "Tracked" << std::endl;
		}
		
        //track target boundary box
        BBox_c bb;
        bb = tracker.getBBox();

        //BBox --> cv::Rect
        // (x,y) center --> (x,y) left corner (for visualizing)
        if (is_verbose) std::cout << "BBox --> cv::Rect" << std::endl;
        float inv_resize_param = 1.f/kResizeImageParameter;
        bb_ocv = cv::Rect( (bb.cx - bb.w/2.)*inv_resize_param,
                           (bb.cy - bb.h/2.)*inv_resize_param,
                            bb.w*inv_resize_param,
                            bb.h*inv_resize_param);                            
        
        //std::cout << "x: " << bb_ocv.x << ", y: " << bb_ocv.y << ", w: " << bb_ocv.width << ", h: " << bb_ocv.height << std::endl; // if you want to result on the screen

        //estimate distance & direction
#if defined( YMC_CAM )  || defined( USE_VIDEO ) 
		// for GC cam and recorded videos, using average distance
        dist = getDistanceAverageFromDisparity(disp, bb_ocv, kDepthMinRangeValue, kDepthMaxRangeValue); // z[mm]  
        //dist = getDistanceMinimumFromDisparity(disp, bb_ocv, kDepthMinRangeValue, kDepthMaxRangeValue); // z[mm]  
#elif defined( ZED_CAM )
		// for ZED, using the minimum distance
		int x_min, y_min;
		dist = estimateDistanceMinimum(disp, bb_ocv, kDepthMinRangeValue, kDepthMaxRangeValue, &x_min, &y_min); // z[mm]
#endif  
		if (dist > kDepthMinRangeValue) p_dist = dist; // latest value
		
		if (dist < kDepthMinRangeValue)
		{
			// std::cout << "dist1: " << dist << std::endl;
		 	dist = p_dist + 0.1; // add a little noise, so the controller doesn't think it keeps getting the same value
			// std::cout << "dist2: " << dist << std::endl;
		}
		
		// center of the bounding box, resize to the actual scale
        pos_x = bb.cx * inv_resize_param;		 // x[pix]
        pos_y = bb.cy * inv_resize_param;		 // y[pix]

        //output to microautobox
        float val = dist;        
        float val2 = pos_x;
        float val3 = pos_y;
#ifdef UDP
        uint8_t msg[12];
        memcpy(msg,&val,4);
        memcpy(msg+4,&val2,4);
        memcpy(msg+8,&val3,4);

        if (udp.initYet != 1) 
        {
        	udp.init("10.1.1.100","10.1.1.101",5001,5001);
        }
        udp.sendPacket(msg,sizeof(msg));
#endif        
        std::cout << "z: " << val << ", x: " << val2 << ", y: " << val3 << std::endl;
        // end of output to microautobox
        
        //draw information
        if (is_verbose) std::cout << "drawing info" << std::endl;
		char string_buf[256];
		
		cv::rectangle(Image, bb_ocv, tracker_color, kLineThickness); // bounding box

        std::sprintf(string_buf, "x[pix]: %4d", static_cast<int>(pos_x));
        cv::putText(Image, string_buf, cv::Point(100,75), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,0,255), 2); // x val

        std::sprintf(string_buf, "y[pix]: %4d", static_cast<int>(pos_y));
        cv::putText(Image, string_buf, cv::Point(100,100), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,0,255), 2); // y val

        std::sprintf(string_buf, "z[m]  : %4.2f", dist/1000);
        cv::putText(Image, string_buf, cv::Point(100,125), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,0,255), 2); // distance

#if defined( ZED_CAM )
        cv::circle(Image, cv::Point(x_min, y_min), 2, tracker_color, 2); // show the minimum distance point for ZED camera
#endif
        
        float fps = cv::getTickFrequency() / ((double)cv::getTickCount() - timer); // calculate fps
        cv::putText(Image, "FPS : " + SSTR(int(fps)), cv::Point(100,50), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,0,255), 2); // overlay fps
        
        // for overlaying the trajectory points
        if(trajectory1.traj_x_process.size() > 0)
		{
        	for(int i = 0; i < trajectory1.traj_x_process.size(); i++)
        	{
        		cv::circle(Image, cv::Point(trajectory1.traj_x_process.at(i), trajectory1.traj_y_process.at(i)), i+1, cv::Scalar(0,165+i*10,0), i+1);
        		cv::circle(Image, cv::Point(trajectory2.traj_x_process.at(i), trajectory2.traj_y_process.at(i)), i+1, cv::Scalar(0,165+i*10,0), i+1);
        	}  
        }     
        
        //show tracking image
        if (is_verbose) std::cout << "Show Images" << std::endl;
        cv::imshow( "Tracker", Image ); // Image
        
#ifdef SHOW_DEPTH // for showing the depth map
#if defined ( YMC_CAM )
        cv::cvtColor(disp, depth, cv::COLOR_GRAY2BGR);
#elif defined ( ZED_CAM )
		cv::Mat depth_gray;
		cv::cvtColor(depth, depth_gray, cv::COLOR_BGRA2GRAY);
#endif
        cv::rectangle(depth, bb_ocv, tracker_color, kLineThickness); // Image
        cv::imshow( "Depth", depth_gray);
#endif

        //keyboard command
        if (is_verbose) std::cout << "keyboard command" << std::endl;
        int key = 0;
        key = 0xFF & cv::waitKey(1);
        
        if (is_verbose) std::cout << "keyboard pressed" << std::endl;
        
        // End the program if ESC is used
        if( key == 27 ) 
        {
            break;
        }

        // restart if 'r' is pressed
        if( key == 'r' )
        {
            mode = MODE_DETECTING;
            select_flag = NON_SELECT;
            select_point = cv::Point();
            is_detector_init = 0;
            is_tracker_init = 0;
            cv::destroyWindow("Depth");
            cv::destroyWindow("Tracker");
            std::cout << "restart" << std::endl;
            continue;
        }
        
        // pause if 'p' is pressed
        if( key == 'p')
        {	
        	std::cout << "paused" << std::endl;
    		while(cv::waitKey(1) != 'p')
    		{
    			std::cout << "resume" << std::endl;
    		}
    	}
        
        if (is_verbose) std::cout << "End Loop\n" << std::endl;
    }
    // For safely termination
    cv::destroyWindow("Depth");
    cv::destroyWindow("Detector");
    cv::destroyWindow("Tracker");

    return 0; // terminated
    
    while(1); // Thie loop does nothing, but smiling at you :)
}

/* END */

