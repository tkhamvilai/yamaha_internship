#include "trajectory.hpp"

TRAJECTORY::TRAJECTORY() // constructor
{

}

void TRAJECTORY::store_point(float pos_x, float pos_y) // storing points, the first index it the oldest one. the last index is the newest
{
	if (this->traj_x.size() == HISTORY_DATA_NUM) // if already reached the maximum number of stored points, deleted the oldest one
	{		
		for (unsigned int i = 0; i < this->traj_x.size() - 1; i++) // this loops shift the store points and store the new point
		{
			this->traj_x.at(i) = this->traj_x.at(i+1); // shift x trajectory
			this->traj_y.at(i) = this->traj_y.at(i+1); // shift y trajectory
		}
		
		this->traj_x.at(this->traj_x.size()-1) = pos_x; // store new x trajectory
		this->traj_y.at(this->traj_y.size()-1) = pos_y; // store new y trajectory
	}
	else // if haven't reach the maximum number of stored points
	{
		this->traj_x.push_back(pos_x); // store new x trajectory
		this->traj_y.push_back(pos_y); // store new y trajectory
	}
}

void TRAJECTORY::print_points() // print the processed points
{
	std::cout << "traj_x:";
	for (unsigned int i = 0; i < this->traj_x_process.size(); i++)
	{
		std::cout << ' ' << this->traj_x_process[i]; // print x trajectory
	}
	std::cout << '\n';
	
	std::cout << "traj_y:";
	for (unsigned int i = 0; i < this->traj_y_process.size(); i++)
	{
		std::cout << ' ' << this->traj_y_process[i]; // print y trajectory
	}
	std::cout << '\n';
}

void TRAJECTORY::process_points(int w, int h) // process a raw trajctory
{	
	// clear old processed data
	this->traj_x_process.clear();
	this->traj_y_process.clear();
	
	if(this->traj_x.size() > 0) // if there are raw data to be processed
	{
		for (unsigned int i = 0; i < this->traj_x.size(); i++)
		{
			this->traj_x_process.push_back(this->traj_x.at(i)); // don't need to processed x, the coordinate is already correct
			this->traj_y_process.push_back( this->traj_y.at(i) + (h - this->traj_y.at(i)) / (this->traj_y.size()-1) * (this->traj_y.size()-i) ); // process y so that we can see the trajectory. Tf they aren't processed, we will see points stacking on top of each other.
		}
	}
}
