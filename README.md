# TK's Yamaha Internship

This repository includes three software: Baby Ducking, Obstacle Detection, adnd Real-time Video Stitiching.

## Prerequisites

What things you need to install the software and how to install them

### OpenCV 3.4.6 Library

* This will not work with OpenCV 2.x.x

* Remove old stuff
```
sudo apt-get purge libopencv*
sudo apt-get purge python-numpy
sudo apt autoremove
```

* Update the system
```
sudo apt-get update
sudo apt-get dist-upgrade
```

* Install Dependencies
```
sudo apt-get install --only-upgrade g++-5 cpp-5 gcc-5
sudo apt-get install build-essential make cmake cmake-curses-gui \
                       g++ libavformat-dev libavutil-dev \
                       libswscale-dev libv4l-dev libeigen3-dev \
                       libglew-dev libgtk2.0-dev
sudo apt-get install libdc1394-22-dev libxine2-dev \
                       libgstreamer1.0-dev \
                       libgstreamer-plugins-base1.0-dev
sudo apt-get install libjpeg8-dev libjpeg-turbo8-dev libtiff5-dev \
                       libjasper-dev libpng12-dev libavcodec-dev
sudo apt-get install libxvidcore-dev libx264-dev libgtk-3-dev \
                       libatlas-base-dev gfortran
sudo apt-get install libopenblas-dev liblapack-dev liblapacke-dev
sudo apt-get install qt5-default
sudo apt-get install python3-dev python3-pip python3-tk
sudo pip3 install numpy
sudo pip3 install matplotlib
```

* Download OpenCV and its extra modules
```
cd
cd Downloads
wget https://github.com/opencv/opencv/archive/3.4.6.zip \
       -O opencv-3.4.6.zip
unzip opencv-3.4.6.zip
wget https://github.com/opencv/opencv_contrib/archive/3.4.6.zip -o opencv_contrib.zip
unzip opencv_contrib.zip
```

* Change the source code a bit to allow GPU working correctly on Stitching
```
nano ~/Downloads/opencv-3.4.6/modules/stitching/src/stitcher.cpp
```

* Comment the line 68
> //stitcher.setSeamFinder(makePtr<detail::GraphCutSeamFinderGpu>());

* Add this below that line
> stitcher.setSeamFinder(makePtr<detail::GraphCutSeamFinder>());

* Build and Install OpenCV
```
cd
cd ~/Downloads/opencv-3.4.6
mkdir build
cd build
cmake \
    -D CMAKE_BUILD_TYPE=Release \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D BUILD_PNG=ON \
    -D BUILD_TIFF=OFF \
    -D BUILD_TBB=OFF \
    -D BUILD_JPEG=ON \
    -D BUILD_JASPER=OFF \
    -D BUILD_ZLIB=OFF \
    -D BUILD_EXAMPLES=ON \
    -D BUILD_opencv_java=OFF \
    -D BUILD_opencv_nonfree=OFF \
    -D BUILD_opencv_python=ON \
	-D BUILD_opencv_python3=ON \
    -D ENABLE_PRECOMPILED_HEADERS=OFF \
    -D WITH_OPENCL=ON \
    -D WITH_OPENMP=OFF \
    -D WITH_FFMPEG=ON \
    -D WITH_GSTREAMER=ON \
    -D WITH_GSTREAMER_0_10=OFF \
    -D WITH_CUDA=ON \
    -D WITH_NVCUVID=ON \
    -D WITH_GTK=ON \
    -D WITH_VTK=OFF \
    -D WITH_TBB=ON \
    -D WITH_1394=OFF \
    -D WITH_OPENEXR=OFF \
    -D CUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-9.0 \
    -D CUDA_ARCH_BIN=6.2 \
    -D CUDA_ARCH_PTX="" \
    -D INSTALL_C_EXAMPLES=OFF \
    -D INSTALL_TESTS=OFF \
    -D OPENCV_ENABLE_NONFREE=ON \
	-D WITH_CUBLAS=ON -D ENABLE_FAST_MATH=ON -D CUDA_FAST_MATH=ON \
	-D WITH_QT=ON -D WITH_OPENGL=ON \
	-D OPENCV_EXTRA_MODULES_PATH=~/Downloads/opencv_contrib/modules \
    -D OPENCV_TEST_DATA_PATH=../opencv_extra/testdata \
    ..
make -j4
sudo make install
```

### CUDA (GPU Accelerated) Library

* Follow this official [guide](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html)

### ZED Camera Library

* Follow this official [guide](https://www.stereolabs.com/docs/getting-started/installation/)

### Darknet (Yolov3) Library

* This library is already include in this repository. It has been customized a bit
* The original repository is [here](https://github.com/pjreddie/darknet)

## Build and Execute the Software

* Clone this repository and compile it
```
cd
cd Documents
git clone https://bitbucket.org/tkhamvilai/yamaha_internship/src/master/
mkdir build
cd build
cmake ..
make -j4
cd ../bin
```

* There will be three executable files inside bin folder. Each of them corresponds to each software as its name implies.
* Use the regular method to execute i.e.
```
./<file name> -j4
```

## Troubleshoot

* contact tkhamvilai3@gatech.edu
