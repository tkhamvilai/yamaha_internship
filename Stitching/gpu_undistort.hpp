// include opencv libraries
#include <opencv2/opencv.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/core/utility.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

class GPU_UNDISTORT // gpu undistort class
{
	public:
		GPU_UNDISTORT(); // constructor
		cv::Mat process_image(cv::Mat frame); // undistort function
		void camera_info(cv::Mat cam_mat, cv::Mat dist_coeff, int w, int h); // a function for loading the camera calibration data
		cv::cuda::GpuMat mapx_; // gpu matrix
		cv::cuda::GpuMat mapy_; // gpu matrix
		bool camera_set_; // whether the camera is set
};
