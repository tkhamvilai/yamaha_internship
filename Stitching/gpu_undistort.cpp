#include "gpu_undistort.hpp"

GPU_UNDISTORT::GPU_UNDISTORT() // object constructor
{
	camera_set_ = false; // camera is not yet set
}

cv::Mat GPU_UNDISTORT::process_image(cv::Mat frame) // undistort function
{
	if(!this->camera_set_) // if the camera is still not set
	{
		return frame; // terminate
    }
	cv::cuda::GpuMat image_gpu(frame); // load cpu image to gpu image
    cv::cuda::GpuMat image_gpu_rect(cv::Size(frame.rows, frame.cols), frame.type()); // select area to undistort
    cv::cuda::remap(image_gpu, image_gpu_rect, this->mapx_, this->mapy_, cv::INTER_LINEAR, cv::BORDER_CONSTANT); // undistort
    cv::Mat image_rect = cv::Mat(image_gpu_rect); // load gpu image to cpu
    
    return image_rect; // output undistorted image
}

void GPU_UNDISTORT::camera_info(cv::Mat cam_mat, cv::Mat dist_coeff, int w, int h) // a function for loading the camera calibration data
{
	cv::Mat m1; // cpu matrix
    cv::Mat m2; // cpu matrix
    cv::initUndistortRectifyMap(cam_mat, dist_coeff, cv::Mat(), cam_mat, cv::Size(w,h), CV_32FC1, m1, m2); // load the camera calibration data
    this->mapx_ = cv::cuda::GpuMat(m1); // load cpu matrix to gpu matrix
    this->mapy_ = cv::cuda::GpuMat(m2); // load cpu matrix to gpu matrix
    this->camera_set_ = true; // the camera is set
}
