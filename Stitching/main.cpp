/***********************************************

	Perception for Stitching Cameras from Thanakorn Khamvilai 2019.06.04
        
***********************************************/

// include opencv libraries
#include <opencv2/opencv.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/core/utility.hpp>
#include "opencv2/cudastereo.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>
#include "opencv2/ximgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/stitching.hpp"

// include c++ standard libraries
#include <iostream>
#include <ctype.h>

#include "gpu_undistort.hpp" // include my ouwn gpu undistort object

#define IS_RECORDING // whether you want to record
//#define USE_CAM_3 // used the 3rd camera (if hardware allowed)
//#define USE_CAM_4 // used the 3rd camera (if hardware allowed)

#define WIDTH_RESOLUTION 640 // width resolution of the camera
#define HEIGHT_RESOLUTION 480 // height resolution of the camera

// Convert to string
#define SSTR( x ) static_cast< std::ostringstream & >( \
( std::ostringstream() << std::dec << x ) ).str()

bool is_verbose = false; // whether you want to see some texts printing (good for debugging)
bool try_use_gpu = true; // whether you want to use gpu
bool divide_images = false; // whether you want to images to be divided
std::vector<cv::Mat> imgs; // vector of images, stores (>=2) images from cameras
cv::Stitcher::Mode stitcher_mode = cv::Stitcher::PANORAMA; // stitching mode

int main( int argc, const char** argv ) // main function
{			
	cv::Mat Image, frame, frame_original, frame2, frame_original2, pano; // image variables
	cv::namedWindow("pano", cv::WINDOW_AUTOSIZE); // for visualizing stithcing result
	
	cv::VideoCapture cap("/dev/video1"); // read the 1st camera
	cv::namedWindow("cam1", cv::WINDOW_AUTOSIZE); // for visualizing an image from the 1st camera
	cv::VideoCapture cap2("/dev/video2"); // read the 2nd camera
	cv::namedWindow("cam2", cv::WINDOW_AUTOSIZE); // for visualizing an image from the 2nd camera
	
#ifdef USE_CAM_3
	cv::Mat frame3, frame_original3; // image variables for the 3rd camera
	cv::VideoCapture cap3("/dev/video3"); // read the 3rd camera
	cv::namedWindow("cam3", cv::WINDOW_AUTOSIZE); // for visualizing an image from the 3rd camera
#endif

#ifdef USE_CAM_4
	cv::Mat frame4, frame_original4; // image variables for the 4th camera
	cv::VideoCapture cap4("/dev/video4"); // read the 4th camera
	cv::namedWindow("cam4", cv::WINDOW_AUTOSIZE); // for visualizing an image from the 4th camera
#endif	
	
	// camera matrix from the camera calibration
	cv::Mat Camera_Matrix = cv::Mat::eye(3, 3, CV_32FC1);
	Camera_Matrix.at<float>(0,0) = 277.77571583;
	Camera_Matrix.at<float>(0,2) = 324.78536451;
	Camera_Matrix.at<float>(1,1) = 277.62427155;
	Camera_Matrix.at<float>(1,2) = 231.45349976;
	
	cv::Mat Distortion_Coefficients = cv::Mat::zeros(1, 5, CV_32FC1);
	Distortion_Coefficients.at<float>(0,0) = -0.35070004;
	Distortion_Coefficients.at<float>(0,1) = 0.16721226;
	Distortion_Coefficients.at<float>(0,2) = 0.00045659;
	Distortion_Coefficients.at<float>(0,3) = -0.00122603;
	Distortion_Coefficients.at<float>(0,4) = -0.04461526;
	
	cv::Mat Camera_Matrix2 = cv::Mat::eye(3, 3, CV_32FC1);
	Camera_Matrix2.at<float>(0,0) = 271.86522219;
	Camera_Matrix2.at<float>(0,2) = 322.87601999;
	Camera_Matrix2.at<float>(1,1) = 271.30300558;
	Camera_Matrix2.at<float>(1,2) = 230.66822175;
	
	cv::Mat Distortion_Coefficients2 = cv::Mat::zeros(1, 5, CV_32FC1);
	Distortion_Coefficients2.at<float>(0,0) = -0.31551838;
	Distortion_Coefficients2.at<float>(0,1) = 0.10660576;
	Distortion_Coefficients2.at<float>(0,2) = -0.000816;
	Distortion_Coefficients2.at<float>(0,3) = -0.00073065;
	Distortion_Coefficients2.at<float>(0,4) = -0.01633807;
	
	// status handler
	cv::Stitcher::Status status = cv::Stitcher::ERR_NEED_MORE_IMGS; // stithcing status
	cv::Stitcher::Status status_transform = cv::Stitcher::ERR_NEED_MORE_IMGS; // camera transformation status
	
	std::cout << "Create Stitcher..." << std::endl;
	cv::Ptr<cv::Stitcher> stitcher = cv::Stitcher::create(stitcher_mode, try_use_gpu); // stitcher object
	
	GPU_UNDISTORT undistort = GPU_UNDISTORT(); // undistort object 1
	undistort.camera_info(Camera_Matrix, Distortion_Coefficients, WIDTH_RESOLUTION, HEIGHT_RESOLUTION); // load calibration data 1 to undistort object 1
	
	GPU_UNDISTORT undistort2 = GPU_UNDISTORT(); // undistort object 2
	undistort2.camera_info(Camera_Matrix2, Distortion_Coefficients2, WIDTH_RESOLUTION, HEIGHT_RESOLUTION); // load calibration data 2 to undistort object 2
	
#ifdef IS_RECORDING
	// For Recording Video //
	cv::VideoWriter video_cam1("cam1.avi", CV_FOURCC('M','J','P','G'), 7, cv::Size(WIDTH_RESOLUTION, HEIGHT_RESOLUTION)); // recording object for the 1st camera
	cv::VideoWriter video_cam2("cam2.avi", CV_FOURCC('M','J','P','G'), 7, cv::Size(WIDTH_RESOLUTION, HEIGHT_RESOLUTION)); // recording object for the 2nd camera
#ifdef USE_CAM_3
	cv::VideoWriter video_cam3("cam3.avi", CV_FOURCC('M','J','P','G'), 7, cv::Size(WIDTH_RESOLUTION, HEIGHT_RESOLUTION)); // recording object for the 3rd camera
#endif
#ifdef USE_CAM_4
	cv::VideoWriter video_cam4("cam4.avi", CV_FOURCC('M','J','P','G'), 7, cv::Size(WIDTH_RESOLUTION, HEIGHT_RESOLUTION)); // recording object for the 4th camera
#endif
	cv::VideoWriter video_cam_pano;  // recording object for the stitching result
#endif

    while(1) // main loop
    {
    	if (is_verbose) std::cout << "Start Loop" << std::endl;
    	double timer = (double)cv::getTickCount(); // keeping track to time for one loop
        
        //camera input
        if (is_verbose) std::cout << "Fetching Image" << std::endl;
#if !defined (USE_CAM_3) && !defined (USE_CAM_4)  
		if(cap.read(frame_original) && cap2.read(frame_original2)) // read the 1st and the 2nd camera
#elif defined (USE_CAM_3)
		if(cap.read(frame_original) && cap2.read(frame_original2) && cap3.read(frame_original3)) // read the 1st, the 2nd, and the 3rd camera
#elif defined (USE_CAM_3) && defined (USE_CAM_4)
		if(cap.read(frame_original) && cap2.read(frame_original2) cap3.read(frame_original3) && cap4.read(frame_original4)) // read the 1st, the 2nd, the 3rd, and the 4th camera
#endif
		{	
			frame = undistort2.process_image(frame_original); // undistort the image from the 1st camera
			imgs.push_back(frame.clone()); // stores the image from the 1st camera
			
			frame2 = undistort2.process_image(frame_original2); // undistort the image from the 2nd camera
			imgs.push_back(frame2.clone()); // stores the image from the 2nd camera

#ifdef USE_CAM_3
			frame3 = undistort2.process_image(frame_original3); // undistort the image from the 3rd camera
			imgs.push_back(frame3.clone()); // stores the image from the 3rd camera
#endif
#ifdef USE_CAM_4
			frame4 = undistort2.process_image(frame_original4); // undistort the image from the 4th camera
        	imgs.push_back(frame4.clone()); // stores the image from the 4th camera
#endif
        	   		
    		if(status_transform != cv::Stitcher::OK) // if haven't yet calculated the camera transformation (do only one time)
    		{
    			std::cout << "Estimate Transformation" << std::endl;
    			status_transform = stitcher->estimateTransform(imgs); // calculate the camera transformation
    			stitcher->composePanorama(imgs, pano); // stithcing after calculating the camera transformation
    			video_cam_pano = cv::VideoWriter("cam_pano.avi", CV_FOURCC('M','J','P','G'), 7, cv::Size(pano.cols, pano.rows)); // recording object for the stitching result
    		}
    		if(status_transform == cv::Stitcher::OK) // if already calculated the camera transformation (do only one time)
			{	
				std::cout << "Stitching..." << std::endl;
				status = stitcher->composePanorama(imgs, pano); // stithcing using the same camera transformation
			}    		
    		imgs.clear(); // reset stored images for the next loop
        }  
        else
        {
        	std::cout << "Cannot Get Images from Videos" << std::endl;
        	return 1; // terminate if cannot read cameras
        }

#ifdef IS_RECORDING
		video_cam1.write(frame); // recording the 1st camera      
        video_cam2.write(frame2); // recording the 2nd camera
#ifdef USE_CAM_3
		video_cam3.write(frame3); // recording the 3rd camera      
#endif
#ifdef USE_CAM_4
		video_cam4.write(frame4); // recording the 4th camera      
#endif
#endif
        
        cv::imshow("cam1", frame ); // visualizing the 1st camera
        cv::imshow("cam2", frame2 ); // visualizing the 2nd camera        
#ifdef USE_CAM_3
        cv::imshow("cam3", frame3 ); // visualizing the 3rd camera
#endif
#ifdef USE_CAM_4
        cv::imshow("cam4", frame4 ); // visualizing the 4th camera
#endif        
        
        if (status == cv::Stitcher::OK) // if successfully stithcing
        {
        	float fps = cv::getTickFrequency() / ((double)cv::getTickCount() - timer); // calculate frames per second
        	cv::putText(pano, "FPS : " + SSTR(int(fps)), cv::Point(100,50), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,0,255), 2); // overlay text
        	std::cout << "Stitiching Successful" << std::endl;
        	video_cam_pano.write(pano); // recording the stitching result
        	cv::imshow("pano", pano ); // visualizing the stitching result
        }
        else
        {
        	std::cout << "Can't stitch images, error code = " << int(status) << std::endl;
        }        
        
        // hit ESC to terminate the software
        int key = 0;
        key = 0xFF & cv::waitKey(1);
        if( key == 27 ) 
        {
            break;
        }
    }
    // For safely termination
    cv::destroyWindow("cam1");
    cv::destroyWindow("cam2");
#ifdef USE_CAM_3
    cv::destroyWindow("cam3");
#endif
#ifdef USE_CAM_4
    cv::destroyWindow("cam4");
#endif
    cv::destroyWindow("pano");

    return 0; // terminated
    
    while(1); // Thie loop does nothing, but smiling at you :)
}

/* END */

