/*!
 *	@file 	ymc_stereo_camera.h
 *	@brief	YMCStereoCamrea class
 *	@date	2019.04.04
 *	ver2	delete left_img & right_img & buff_img_ptr (because no use)
 */

#ifndef _YMC_STEREO_CAMERA_H_
#define _YMC_STEREO_CAMERA_H_

#include "cam_driver.h"
#include <iostream>
#include <cstring>

class YMCStereoCamera
{
private:
	/* define */
	const int kImageWidth_    = CAM_IMG_WIDTH;
	const int kImageHeight_   = CAM_IMG_HEIGHT;
	const int kImageSize_     = kImageWidth_ * kImageHeight_;
	const int kBuffImageSize_ = kImageSize_ * CAMERA_NUM;
	
	libusb_device         *camera_device   = NULL;
	libusb_device        **usb_device_list = NULL;
	libusb_device_handle  *device_handle   = NULL;
	
	unsigned char *buff_img = nullptr;
	
	bool is_run_capture = false;
public:
	YMCStereoCamera();
	~YMCStereoCamera();
	
	bool open();
	bool isOpened();
	bool getImage(unsigned char* left, unsigned char* right, const int width, const int height);
	bool close();
	
	void dump();
	const int height()const;
	const int width()const;
};

#endif	/*_YMC_STEREO_CAMERA_H_*/
