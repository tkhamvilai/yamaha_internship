/*!
 *	@file	stereo_rectify.h
 *	@brief	ステレオカメラ用　歪み補正クラス
 *	@author	takuma nishi
 *	@date	2019.04.03
 */
#pragma once
#include <iostream>	/* dump, error */
#include <iomanip>	/* dump */
#include <bitset>	/* dump */
#include <algorithm>/* std::max*/
#include <string>	/* dump, error*/
#include <fstream>	/* write&read data */


class StereoRectify
{
private:
	/* image size */
	const int kImageWidth_;
	const int kImageHeight_;
	const int kImageSize_;

	/* fixed point */
	const size_t			kFixedPoint_;
	const unsigned short	kDecimalCoeff_;
	const unsigned short	kFixedCoeff_;
	const unsigned short	kFixedLimit_;

	/* map buffer */
	unsigned short* left_x_map_ = nullptr;
	unsigned short* left_y_map_ = nullptr;
	unsigned short* right_x_map_ = nullptr;
	unsigned short* right_y_map_ = nullptr;

	/* member function */
	const size_t calcFixedPoint(const int image_width, const int image_height);
	bool allocateMemory();
	void make16BitMap(const float* const src_map, unsigned short* const dst_map, const unsigned short offset);
	void computeRectify(const unsigned char* const src, unsigned char* const dst, const unsigned short* const x_map, const unsigned short* const y_map, const unsigned short x_offset, const unsigned short y_offset, const int color_step);
public:
	StereoRectify(const int image_width, const int image_height);
	~StereoRectify();

	void finalize();

	void dump();

	bool initialize(
		const float* const left_x_map, const float* const left_y_map,
		const float* const right_x_map, const float* const right_y_map,
		const int image_width, const int image_height);

	bool isInitialize();
	bool writeMap(const std::string& file_name);
	bool readMap(const std::string& file_name);
	bool process(
		const unsigned char* const left_image, const unsigned char* const right_image,
		unsigned char* const left_rectify, unsigned char* const right_rectify,
		const int image_width, const int image_height, const int color = 1);

	/* TEST
	unsigned short float2fixed(const float src);
	float fixed2float(const unsigned short src);

	void test(const float* const l_x_map, const int image_width, const int image_height);
	*/
};