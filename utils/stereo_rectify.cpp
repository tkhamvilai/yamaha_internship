#include "stereo_rectify.h"

const size_t StereoRectify::calcFixedPoint(const int image_width, const int image_height)
{
	const int length = std::max(image_width, image_height);
	if (length > static_cast<int>(std::numeric_limits<unsigned short>::max()))
	{
		return 0;
	}
	
	size_t fixed_point = 0;
	for (int integer_max = 1; integer_max < length; integer_max <<= 1, fixed_point++);

	size_t res = sizeof(unsigned short) * 8 - fixed_point;
	return res;
}

StereoRectify::StereoRectify(const int image_width, const int image_height)
	: kImageWidth_(image_width)
	, kImageHeight_(image_height)
	, kImageSize_(kImageWidth_*kImageHeight_)
	, kFixedPoint_(calcFixedPoint(kImageWidth_, kImageHeight_))
	, kDecimalCoeff_(static_cast<unsigned short>(pow(2, kFixedPoint_)))
	, kFixedCoeff_(kDecimalCoeff_ - 1)
	, kFixedLimit_(static_cast<unsigned short>(pow(2, sizeof(unsigned short)*8 - kFixedPoint_)))
	, left_x_map_(nullptr)
	, left_y_map_(nullptr)
	, right_x_map_(nullptr)
	, right_y_map_(nullptr)
{
	if (kImageWidth_ <= 0 || kImageHeight_ <= 0)
	{
		std::cout << "Error: StereoRectify::StereoRectify(width, height)\n";
		std::cout << "     width > 0 and height > 0\n";
	}
}

StereoRectify::~StereoRectify()
{
	finalize();
}

void StereoRectify::finalize()
{
	if (left_x_map_  != nullptr) { delete[] left_x_map_;  left_x_map_  = nullptr; }
	if (left_y_map_  != nullptr) { delete[] left_y_map_;  left_y_map_  = nullptr; }
	if (right_x_map_ != nullptr) { delete[] right_x_map_; right_x_map_ = nullptr; }
	if (right_y_map_ != nullptr) { delete[] right_y_map_; right_y_map_ = nullptr; }
}

bool StereoRectify::allocateMemory()
{
	if (kImageWidth_ <= 0 || kImageHeight_ <= 0)
		return false;

	finalize();

	try
	{
		left_x_map_  = new unsigned short[kImageSize_];
		left_y_map_  = new unsigned short[kImageSize_];
		right_x_map_ = new unsigned short[kImageSize_];
		right_y_map_ = new unsigned short[kImageSize_];
	}
	catch(...)
	{
		std::cout << "error: StereoRectify::allocateMemory:\n";
		std::cout << "メモリ確保に失敗しました\n";
		finalize();
		return false;
	}

	return true;
}

void StereoRectify::make16BitMap(
	const float* const src_map, unsigned short* const dst_map, const unsigned short offset)
{
	const unsigned short	table_start = offset << kFixedPoint_;

	const float*	src_ptr = src_map;
	unsigned short* dst_ptr = dst_map;

	for (int y = 0; y < kImageHeight_; y++)
	{
		for (int x = 0; x < kImageWidth_; x++)
		{
			unsigned short integer = static_cast<unsigned short>(*src_ptr);
			float decimal = *src_ptr++ - static_cast<float>(integer);

			*dst_ptr++ = (integer << kFixedPoint_) + table_start + static_cast<unsigned short>(decimal * kDecimalCoeff_);
		}
	}
}

bool StereoRectify::initialize(
	const float* const left_x_map, const float* const left_y_map,
	const float* const right_x_map, const float* const right_y_map,
	const int image_width, const int image_height)
{
	if (image_height != kImageHeight_) return false;
	if (image_width != kImageWidth_) return false;
	
	if (allocateMemory() == false) return false;

	make16BitMap(left_x_map,  left_x_map_,  (kFixedLimit_ - kImageWidth_)  / 2);
	make16BitMap(left_y_map,  left_y_map_,  (kFixedLimit_ - kImageHeight_) / 2);
	make16BitMap(right_x_map, right_x_map_, (kFixedLimit_ - kImageWidth_)  / 2);
	make16BitMap(right_y_map, right_y_map_, (kFixedLimit_ - kImageHeight_) / 2);

	return true;
}

bool StereoRectify::isInitialize()
{
	return ((left_x_map_  != nullptr)
		 && (left_y_map_  != nullptr)
		 && (right_x_map_ != nullptr)
		 && (right_y_map_ != nullptr));
}

bool StereoRectify::writeMap(const std::string& file_name)
{
	if (isInitialize() == false) return false;

	// file open with binary mode
	std::ofstream output_file(file_name, std::ios::out | std::ios::binary);

	try
	{
		if (output_file.is_open() == false) throw;

		output_file.write((const char*)(left_x_map_), sizeof(unsigned short)*kImageSize_);
		output_file.write((const char*)(left_y_map_), sizeof(unsigned short)*kImageSize_);
		output_file.write((const char*)(right_x_map_), sizeof(unsigned short)*kImageSize_);
		output_file.write((const char*)(right_y_map_), sizeof(unsigned short)*kImageSize_);
	}
	catch (...)
	{
		std::cout << "could not write Map data\n";
		output_file.close();
		return false;
	}

	output_file.close();
	return true;
}

bool StereoRectify::readMap(const std::string& file_name)
{
	if (allocateMemory() == false) return false;

	// file open with binary mode
	std::ifstream input_file(file_name, std::ios::in | std::ios::binary);
	
	try
	{
		// 開いているかの確認
		if (input_file.is_open() == false)
			throw "could not open file";

		// ファイルサイズの取得
		input_file.seekg(0, std::ios_base::end);
		size_t file_size = input_file.tellg();
		size_t buff_size = (sizeof(unsigned short)*kImageSize_ * 4);
		if (file_size != buff_size)
		{
			std::cout << "file size:" << file_size << "\n";
			std::cout << "buff size:" << buff_size << "\n";
			throw "file size is missmatch!";
		}

		// ファイルの初期位置に戻る
		//input_file.clear();
		input_file.seekg(0, std::ios_base::beg);
				
		// 読込
		input_file.read((char*)left_x_map_, sizeof(unsigned short)*kImageSize_);
		input_file.read((char*)left_y_map_, sizeof(unsigned short)*kImageSize_);
		input_file.read((char*)right_x_map_, sizeof(unsigned short)*kImageSize_);
		input_file.read((char*)right_y_map_, sizeof(unsigned short)*kImageSize_);
	}
	catch (std::string& error_string)
	{
		std::cout << error_string << std::endl;
		finalize();
		input_file.close();
		return false;
	}
	catch (...)
	{
		std::cout << "could not read file [ " << file_name << " ]\n";
		input_file.close();
		finalize();
		return false;
	}

	input_file.close();
	return true;
}

void StereoRectify::dump()
{
	std::cout << "size\n";
	std::cout << "    width : " << kImageWidth_ << "\n";
	std::cout << "    height: " << kImageHeight_ << "\n";
	std::cout << "    size  : " << kImageSize_ << "\n";
	std::cout << "\n";
	std::cout << "pointer\n";
#define DUMP_COUT(A) \
	std::cout << "    " << std::setw(12) << std::left << #A << ": ";\
	if (A == nullptr) std::cout << "nullptr";\
	else              std::cout << A;\
	std::cout << "\n";

	DUMP_COUT(left_x_map_);
	DUMP_COUT(left_y_map_);
	DUMP_COUT(right_x_map_);
	DUMP_COUT(right_y_map_);
#undef DUMP_COUT
	std::cout << "\n";
	std::cout << "fixed point\n";
	std::cout << "    kFixedPoint_  : " << std::setw(6) << kFixedPoint_ << "\n";
	std::cout << "    kDecimalCoeff_: " 
		<< std::setw(6) << kDecimalCoeff_ 
		<< " ( 0x" << std::hex << kDecimalCoeff_ << " | "
	    << std::bitset<16>(kDecimalCoeff_) << " )\n";
	std::cout << "    kFixedCoeff_  : " 
		<< std::setw(6) << std::dec << kFixedCoeff_ 
		<< " ( 0x" << std::hex << kFixedCoeff_ << " | "
		<< std::bitset<16>(kFixedCoeff_) << " )\n";
	std::cout << "    kFixedLimit_  : " << std::setw(6) << std::dec << kFixedLimit_ << "\n";
	std::cout << "\n";
}


void StereoRectify::computeRectify(
	const unsigned char* const src,
	unsigned char* const dst,
	const unsigned short* const x_map,
	const unsigned short* const y_map,
	const unsigned short x_offset,
	const unsigned short y_offset,
	const int color_step)
{
	// offsetを固定少数の整数に桁をあわせ
	const unsigned short x_table_start = x_offset << kFixedPoint_;
	const unsigned short y_table_start = y_offset << kFixedPoint_;

	// 終端を固定少数の整数に桁あわせ
	const unsigned short x_table_end = (kImageWidth_ - 2 + x_offset) << kFixedPoint_;
	const unsigned short y_table_end = (kImageHeight_ - 2 + y_offset) << kFixedPoint_;

	// 
	const unsigned short* x_map_ptr = x_map;
	const unsigned short* y_map_ptr = y_map;
	unsigned char* dst_ptr = dst;

	const int y_step = kImageWidth_ * color_step;
	const int to_int = kFixedPoint_ * 2;

	for (int y = 0; y < kImageHeight_; y++)
	{
		for (int x = 0; x < kImageWidth_; x++)
		{
			unsigned short x_pos = *x_map_ptr++;
			unsigned short y_pos = *y_map_ptr++;
			
			if (x_pos < x_table_start || x_table_end < x_pos
				|| y_pos < y_table_start || y_table_end < y_pos)
			{
				for (int c = 0; c < color_step; c++)
				{
					*dst_ptr++ = 0;
				}
			}
			else
			{
				unsigned short decimal_x = x_pos & kFixedCoeff_;
				unsigned short decimal_y = y_pos & kFixedCoeff_;
				unsigned short integer_x = (x_pos >> kFixedPoint_) - x_offset;
				unsigned short integer_y = (y_pos >> kFixedPoint_) - y_offset;

				for (int c = 0; c < color_step; c++)
				{
					const unsigned char* src_ptr0 = src + integer_y * y_step + integer_x * color_step + c;
					const unsigned char* src_ptr1 = src_ptr0 + y_step;

					int sum  = *src_ptr0 * (kDecimalCoeff_ - decimal_x)*(kDecimalCoeff_ - decimal_y);
						src_ptr0 += color_step;
					    sum += *src_ptr0 *                   decimal_x *(kDecimalCoeff_ - decimal_y);
						sum += *src_ptr1 * (kDecimalCoeff_ - decimal_x)*                  decimal_y;
						src_ptr1 += color_step;
						sum += *src_ptr1 *                   decimal_x *                  decimal_y;

					*dst_ptr++ = (sum >> to_int);
				}
			}
		}
	}
}

bool StereoRectify::process(
	const unsigned char* const left_image,
	const unsigned char* const right_image,
	unsigned char* const left_rectify,
	unsigned char* const right_rectify,
	const int image_width,
	const int image_height,
	const int color)
{
	if (isInitialize() == false) return false;
	if (image_width  != kImageWidth_)  return false;
	if (image_height != kImageHeight_) return false;
	if (color < 0) return false;
	
	computeRectify( left_image,
		            left_rectify,
		            left_x_map_,
		            left_y_map_,
		            (kFixedLimit_ - kImageWidth_) / 2,
		            (kFixedLimit_ - kImageHeight_) / 2,
		            color);

	computeRectify( right_image,
					right_rectify,
					right_x_map_,
					right_y_map_,
					(kFixedLimit_ - kImageWidth_) / 2,
					(kFixedLimit_ - kImageHeight_) / 2,
					color);

	return true;
}


/* for TEST
unsigned short StereoRectify::float2fixed(const float src)
{
	if (src < 0 || kFixedLimit_ < src)
		return 0;

	unsigned short integer = static_cast<unsigned short>(src);
	float decimal = src - static_cast<float>(integer);

	return (integer << kFixedPoint_) + static_cast<unsigned short>(decimal * kDecimalCoeff_);
}

float StereoRectify::fixed2float(const unsigned short src)
{
	unsigned short decimal = src & kFixedCoeff_;
	unsigned short integer = src >> kFixedPoint_;

	float dst = static_cast<float>(integer);
	dst += static_cast<float>(decimal) / static_cast<float>(kDecimalCoeff_);

	return dst;
}

#include <opencv2/opencv.hpp>
void StereoRectify::test(const float* const l_x_map, const int image_width, const int image_height)
{
	if (isInitialize() == false) return;
	if (image_width != kImageWidth_) return;
	if (image_height != kImageHeight_) return;
	const float* src_ptr = l_x_map;
	const unsigned short* map_ptr = left_x_map_;

	cv::Mat error(image_height, image_width, CV_8U);
	for (int i = 0; i < image_height; i++)
	{
		for (int j = 0; j < image_width; j++)
		{
			
			float tmp = fixed2float(*map_ptr++ - ((kFixedLimit_ - kImageWidth_) / 2 << kFixedPoint_));

			tmp = abs(tmp - *src_ptr++);
			std::cout << tmp << std::endl;
		}
	}

	

}
*/
