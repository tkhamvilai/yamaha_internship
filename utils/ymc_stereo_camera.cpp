/*!
 *	@file 	ymc_stereo_camera.cpp
 *	@brief	YMCStereoCamrea class
 *	@date	2019.04.04
 *	ver2	delete left_img & right_img & buff_img_ptr (because no use)
 */
#include "ymc_stereo_camera.h"

YMCStereoCamera::YMCStereoCamera()
	:camera_device(NULL)
	,usb_device_list(NULL)
	,device_handle(NULL)
	,buff_img(nullptr)
	,is_run_capture(false)
{
	buff_img  = new unsigned char[kBuffImageSize_];
}

YMCStereoCamera::~YMCStereoCamera()
{
	if (buff_img != nullptr)
	{
		delete[] buff_img;
		buff_img = nullptr;
	}
}

bool YMCStereoCamera::open()
{
	int ret;
	
	close();

	try
	{	
		ret = cam_Init(&camera_device, &usb_device_list);
		if (ret == 1 || camera_device == NULL || usb_device_list == NULL) throw;
	
		ret = cam_Open(camera_device, usb_device_list, &device_handle);
		if (ret == 1 || device_handle == NULL) throw;
		
		if (isOpened() == false) throw;
		
		ret = cam_StartCapture(device_handle, camera_device, kImageWidth_, kImageHeight_);
		if (ret == 1) throw;
		
		is_run_capture = true;
	}
	catch(...)
	{
		close();
		return false;
	}

	return is_run_capture;
}

bool YMCStereoCamera::getImage(unsigned char* left, unsigned char* right, const int width, const int height)
{
	if (is_run_capture == false) return false;
	if (width  != kImageWidth_ ) return false;
	if (height != kImageHeight_) return false;
	
	// get camera capture data
	unsigned char* buff_img_ptr = cam_CaptureImage();
	
	// save buffer image
	memcpy(buff_img, buff_img_ptr, sizeof(unsigned char) * kBuffImageSize_);
		
	// get images
	cam_Get_LeftImage(buff_img, left, kImageSize_);
	cam_Get_RightImage(buff_img, right, kImageSize_);
	
	return true;
}

bool YMCStereoCamera::close()
{
	bool ret = true;
	
	if (is_run_capture)
	{
		if (cam_EndCapture() == 1)
		{
			ret = false;
		}
		is_run_capture = false;
	}
	
	if (isOpened())
	{
		if (cam_Close(device_handle) == 1)
		{
			ret = false;
		}
	}
	return ret;
}

bool YMCStereoCamera::isOpened()
{
	return ( camera_device   != NULL
	      && usb_device_list != NULL
	      && device_handle   != NULL );
}

const int YMCStereoCamera::height()const
{
	return kImageHeight_;
}

const int YMCStereoCamera::width()const
{
	return kImageWidth_;
}

void YMCStereoCamera::dump()
{
	std::cout << "libusb\n";
	std::cout << "    camera_device  : ";
	if (camera_device == NULL) std::cout << "NULL\n"; 
	else std::cout << camera_device << "\n";
	std::cout << "    usb_device_list: ";
	if (usb_device_list == NULL) std::cout << "NULL\n"; 
	else std::cout << usb_device_list << "\n";
	std::cout << "    device_handle  : ";
	if (device_handle == NULL) std::cout << "NULL\n"; 
	else std::cout << device_handle << "\n";
	
	std::cout << "\n";
	std::cout << "buff_img: "; 
	if (buff_img     == nullptr) std::cout << "nullptr\n";
	else std::cout << "have address" << "\n";
	
	std::cout << "\n";
	
	std::cout << "is_run_capture: " << ((is_run_capture)?"true":"false") << "\n";
}
