#ifndef USB_DRIVER_H_
#define USB_DRIVER_H_

#include "libusb-1.0/libusb.h"

/* USB_VENDOR・USB_PRODUCT */
#define USB_VENDOR  0x117b
#define USB_PRODUCT 0x0320

/* カメラ画像関連 */
#define CAM_IMG_WIDTH   640
#define CAM_IMG_HEIGHT  480
#define BUF_IMG_HEIGHT	512
#define CAMERA_NUM      2
//#define BUF_IMG_SIZE	CAM_IMG_WIDTH * BUF_IMG_HEIGHT * CAMERA_NUM + BUF_IMG_HEIGHT
#define BUF_IMG_SIZE	CAM_IMG_WIDTH * CAM_IMG_HEIGHT * CAMERA_NUM
#define CAM_IMG_SIZE	CAM_IMG_WIDTH * CAM_IMG_HEIGHT

/* cam driver interface */
#ifdef __cplusplus
extern "C" {
#endif
/* cam function */
int cam_Init(libusb_device **dev, libusb_device ***devs);
int cam_Open(libusb_device *dev, libusb_device **devs, libusb_device_handle **dev_handle);
int cam_Get_LeftImage(unsigned char* dataBuf, unsigned char* leftImage, long imageSize);
int cam_Get_RightImage(unsigned char* dataBuf, unsigned char* rightImage, long imageSize);
int cam_Close(libusb_device_handle *dev_handle);
int cam_GetInfo(libusb_device_handle *dev_handle, unsigned char *pData);
int cam_GetDevStatus(libusb_device_handle *dev_handle, unsigned char *pData);
int cam_StartCapture( libusb_device_handle *dev_handle, libusb_device *dev, int width, int height);
int cam_EndCapture();
unsigned char *cam_CaptureImage();
#ifdef __cplusplus
}
#endif
#endif

