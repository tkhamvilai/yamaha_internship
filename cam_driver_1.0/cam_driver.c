#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "cam_driver.h"

#define CAM_LEFT        0
#define CAM_RIGHT       1

/* bRequest */
#define USBCMD_GETINFO                  0x10
#define USBCMD_GETFORMATINFO            0x11
#define USBCMD_GETSTATUS                0x12
#define USBCMD_SETSTATUS                0x13
#define USBCMD_I2C_READ                 0x14
#define USBCMD_I2C_WRITE                0x15
#define USBCMD_EEPROM_READ              0x16
#define USBCMD_EEPROM_WRITE             0x17
#define USBCMD_DEVICE_READ              0x18
#define USBCMD_DEVICE_WRITE             0x19

/* EEPROM関連 */
#define DW_SIZE_MAX     4096    /* write時の最大サイズ */
#define CTLDATA_SIZE    (64)    /* 0x40 */ 
/* fseek */
#define SEEK_CUR    1
#define SEEK_END    2
#define SEEK_SET    0

#define TIMEOUT (5*100000)        /* リクエストタイムアウト */
#define FILL_ON
//#define DEBUG_CONF_MSG
//#define DEBUG_MSG

#define NBUFFERS 20

unsigned char *imageBuffers[NBUFFERS];
static struct libusb_transfer *xfrlist[NBUFFERS];

static int currentFrame=0;

static int trans_complete=0;

static int kill_handler_thread=1;

static pthread_t handler_thread;

void cam_Find(libusb_device **devs, libusb_device **dev);

/*============== init CAM ===================================*/
/* return code              */
/*      failed   1          */
/*      success  0          */
int cam_Init(libusb_device **dev, libusb_device ***devs)
{
	int r;
	ssize_t cnt;
	r = libusb_init(NULL);
	if (r < 0){
		printf("libusb init error\n");
		return 1;
	}
	cnt = libusb_get_device_list(NULL, devs);
	if (cnt < 0){
		printf("get device list error\n");
		return 1;
	}
	cam_Find(*devs, dev);
	if (dev == NULL){
		printf("device not found\n");
		return 1;
	}
	return 0;
}

/*============== Find USB device ===========================*/
void cam_Find(libusb_device **devs, libusb_device **dev)
{
	libusb_device *t_dev;
	int i = 0, j , k;

	*dev = NULL; 
	while ((t_dev = devs[i++]) != NULL){
		struct libusb_device_descriptor desc;
		int r = libusb_get_device_descriptor(t_dev, &desc);
		if (r < 0){
			printf("can not get device descriptor\n");
		}
		if (desc.idVendor == USB_VENDOR && desc.idProduct == USB_PRODUCT){
			*dev = t_dev;
			break;
		}
	}

#ifdef DEBUG_CONF_MSG
	if (*dev != NULL){
		/* debug */
		struct libusb_config_descriptor *config;
		libusb_get_config_descriptor(*dev, 0, &config);

		const struct libusb_interface *inter;
		const struct libusb_interface_descriptor *interdesc;
		const struct libusb_endpoint_descriptor *epdesc;

		printf("num of Interdaces %d\n", (int)config->bNumInterfaces);
		for (i = 0; i < (int)config->bNumInterfaces; i++){
			inter = &config->interface[i];
			printf("num of altsetting %d\n", inter->num_altsetting);
			for (j = 0; j < inter->num_altsetting; j++){
				interdesc = &inter->altsetting[j];
				printf("num of endpoint %d\n", (int)interdesc->bNumEndpoints);
				for (k = 0; k < (int)interdesc->bNumEndpoints; k++){
					epdesc = &interdesc->endpoint[k];
					printf("address %d\n", (int)epdesc->bEndpointAddress);
				}
			}
		}
		libusb_free_config_descriptor(config);
	}
#endif
	return;
	
}

/*============== cam open ==================================*/
/* return code              */
/*      failed   1          */
/*      success  0          */
int cam_Open(libusb_device *dev, libusb_device **devs, libusb_device_handle **dev_handle)
{
	struct libusb_device_descriptor desc;
	unsigned char str1[64];
	unsigned char str2[64];

	int rtn;
	rtn = libusb_open(dev, dev_handle);
	if (rtn < 0){
		printf("device handle open error\n");
		return 1;
	}
	rtn = libusb_get_device_descriptor(dev, &desc);
	if (rtn < 0){
	}

	rtn = libusb_get_string_descriptor_ascii(*dev_handle, desc.iManufacturer,
			(unsigned char*)str1, sizeof(str1));
#ifdef DEBUG_MSG
	printf("Manufactured : %s\n", str1);
#endif

	rtn = libusb_get_string_descriptor_ascii(*dev_handle, desc.iProduct,
			(unsigned char*)str2, sizeof(str2));
#ifdef DEBUG_MSG
	printf("Product : %s", str2);
#endif

	int config2;
	rtn = libusb_get_configuration(*dev_handle, &config2);
	if (config2 != 1){
		libusb_set_configuration(*dev_handle, 1);
	}
#ifdef DEBUG_MSG
	printf("condif%d\n", config2);
#endif
	libusb_set_configuration(*dev_handle, 2);

	libusb_free_device_list(devs, 1);
	if (libusb_kernel_driver_active(*dev_handle, 0) == 1){
		if (libusb_detach_kernel_driver(*dev_handle, 0) == 0){

		}
	}
	rtn = libusb_claim_interface(*dev_handle, 0);
	if (rtn < 0){
		printf("device interface claim error\n");
		return 1;
	}
	return 0;
}

/* callback for bulk transfer */
void stream_callback(struct libusb_transfer *xfr)
{
	int resubmit = 1;
	int i;
	if (kill_handler_thread){
#ifdef DEBUG_MSG
		fprintf(stderr,"stream_callback cancel %x\n",xfr);
#endif
		return;
	}
	switch (xfr->status)
	{
		case LIBUSB_TRANSFER_COMPLETED:
			if ( xfr->actual_length>0 ) {
				memcpy(imageBuffers[currentFrame],xfr->buffer, xfr->actual_length);
				currentFrame = ++currentFrame % NBUFFERS;
			}
			break;
		case LIBUSB_TRANSFER_CANCELLED:
		case LIBUSB_TRANSFER_NO_DEVICE:
		case LIBUSB_TRANSFER_ERROR:
			resubmit=0;
			for (i = 0; i < NBUFFERS; i++){
#ifdef DEBUG_MSG
				fprintf(stderr,"cancel??: xfrlist %x \n",xfr);
#endif 
			    if ( xfrlist[i]==xfr ) {
				struct libusb_transfer *x = xfrlist[i];
				    xfrlist[i]=NULL;
				    libusb_cancel_transfer(xfr);
				    break;
				}
			}
			break;
		case LIBUSB_TRANSFER_TIMED_OUT:
		case LIBUSB_TRANSFER_STALL:
		case LIBUSB_TRANSFER_OVERFLOW:
			resubmit=0;
#ifdef DEBUG_MSG
			printf("warning: %d\n",xfr->status);
#endif
			break;
	}
	if ( resubmit ) libusb_submit_transfer(xfr);
}

void *user_caller(void *arg) {
	int top = (currentFrame-1+NBUFFERS) % NBUFFERS;
	unsigned char *p = imageBuffers[top];

	return NULL;
}

void *handle_events(void *arg) {
	while(!kill_handler_thread) {
		libusb_handle_events_completed(NULL,&kill_handler_thread);
	}
	return NULL;
}


/*=============== cam start capture ============================*/
/* return code              */
/*      failed   1          */
/*      success  0          */
int cam_StartCapture(
	       	libusb_device_handle *dev_handle,
	       	libusb_device *dev,
		int width,
		int height)
{
#ifdef DEBUG_MSG
	printf("cam_start_capture\n");
#endif
	int i;
	int len = width*height*2;
	const struct libusb_interface *inter;
	const struct libusb_interface_descriptor *interdesc;
	const struct libusb_endpoint_descriptor *epdesc;

	struct libusb_config_descriptor *config;

	kill_handler_thread=0;
#ifdef DEBUG_MSG
	printf("pthread: handle_events\n");
#endif
        pthread_create(&handler_thread, NULL, handle_events, NULL);

	libusb_get_config_descriptor(dev, 0, &config);
	inter = &config->interface[0];
	interdesc = &inter->altsetting[0];
	epdesc = &interdesc->endpoint[0];
	for(i=0;i<NBUFFERS;i++) {
		if (imageBuffers[i] == NULL){
		    imageBuffers[i] = malloc(len);
		}
	}
	for(i=0;i<NBUFFERS;i++) {
		struct libusb_transfer *xfr = libusb_alloc_transfer(0);
		xfrlist[i] = xfr;
		unsigned char *data = malloc(len);
		libusb_fill_bulk_transfer(xfr,
			  dev_handle,
			  epdesc->bEndpointAddress,
			  data,
			  len,
			  stream_callback,
			  NULL,
			  TIMEOUT);
	}
	for(i=0;i<NBUFFERS;i++) {
		struct libusb_transfer *xfr = xfrlist[i];
		if (libusb_submit_transfer(xfr) < 0){
#ifdef DEBUG_MSG
			//error
			printf("error----\n");
#endif
			libusb_free_transfer(xfr);
#ifdef DEBUG_MSG
			printf("bulk read error\n");
#endif
			return 1;
		}
	}
	return 0;
}
/*=================== capture end ===================*/
int cam_EndCapture()
{
	int i;

	kill_handler_thread = 1;
#ifdef XXX
	for (i = 0; i < NBUFFERS; i++){
	    if ( xfrlist[i]==NULL ) continue;
	    struct libusb_transfer *x = xfrlist[i];
	    xfrlist[i]=NULL;
	    libusb_cancel_transfer(x);
	    fprintf(stderr,"cancel: xfrlist %x \n",x);
	}
#endif
	for (i = 0; i < NBUFFERS; i++){
	    if (imageBuffers[i] != NULL){
		    free(imageBuffers[i]);
	    }
	}
	
	int rtn = pthread_join( handler_thread, NULL);
	if (rtn != 0){
		printf("err\n");
	}

	return 0;
}

/* =========== buffer read ========================*/
unsigned char *cam_CaptureImage() {

	unsigned char *p = NULL;
	if (kill_handler_thread){
		return p;
	}
	int top = (currentFrame-1+NBUFFERS) % NBUFFERS;
	p = imageBuffers[top];
	return p;
}

/* =============== Set Left or Right Image data ===========*/
void set_LeftRightImageData(unsigned char* dataBuf, unsigned char* outputbuf, long size, int pos)
{
	int idx;
	for (idx = 0; idx < size; idx++){
		outputbuf[idx] = dataBuf[idx * CAMERA_NUM + pos];
	}
	return;
}
/*================ get left image ========================*/
/* return code             */
/*      failed   1         */
/*      success  0         */
int cam_Get_LeftImage(unsigned char* dataBuf, unsigned char* leftImage, long imageSize)
{
	if (dataBuf == NULL){
		printf("buffer data is NULL\n");
		return 1;
	}
	set_LeftRightImageData(dataBuf, leftImage, imageSize, CAM_LEFT);
	return 0;
}

/*=============== get right image =======================*/
/* return code              */
/*      failed   1          */
/*      success  0          */
int cam_Get_RightImage(unsigned char* dataBuf, unsigned char* rightImage, long imageSize)
{
	if (dataBuf == NULL){
		printf("buffer data is NULL\n");
		return 1;
	}
	set_LeftRightImageData(dataBuf, rightImage, imageSize, CAM_RIGHT);
	return 0;
}

/*================ cam close ==============================*/
/* return code              */
/*      failed   1          */
/*      success  0          */
int cam_Close(libusb_device_handle *dev_handle)
{
	if (libusb_release_interface(dev_handle,0) != 0){
		printf("interface release error\n");
		return 1;
	}
	libusb_close(dev_handle);

	return 0;
}
/*================ contorl trans ==========================*/
int cam_ControlTrans(libusb_device_handle *dev_handle, int *nDat, unsigned char *msg)
{
	int bRequest = nDat[0];
	int bmRequestType = (nDat[1]?0:1) * 0x80 + (2 << 5) + 0;
	int wValue   = nDat[2];
	int wIndex   = nDat[3];
	int wLength  = nDat[4];

	int ret = libusb_control_transfer(dev_handle, bmRequestType, bRequest, wValue, wIndex,
			msg, wLength, TIMEOUT);
	if (ret < 0){
		printf("Control message error\n");
		return 1;
	}
	return 0;
}
/*==================== Get Info ======================================*/
/* return code              */
/*      failed   1          */
/*      success  0          */
int cam_GetInfo(libusb_device_handle *dev_handle, unsigned char *pData)
{
	int vCom[5];
	int nRet;

	vCom[0] = USBCMD_GETINFO;
	vCom[1] = 0;
	vCom[2] = 0;
	vCom[3] = 0;
	vCom[4] = 24;
	
	nRet = cam_ControlTrans(dev_handle, vCom, pData);
	return nRet;
}
/*=================== Get Status ====================================*/
/* return code              */
/*      failed   1          */
/*      success  0          */
int cam_GetDevStatus(libusb_device_handle *dev_handle, unsigned char *pData)
{
	int vCom[5];
	int nRet;

	vCom[0] = USBCMD_GETSTATUS;
	vCom[1] = 0;
	vCom[2] = 0;
	vCom[3] = 0;
	vCom[4] = 7;
	nRet = cam_ControlTrans(dev_handle, vCom, pData);
	return nRet;
}


